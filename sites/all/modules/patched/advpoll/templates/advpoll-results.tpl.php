<?php

/**
 * @file
 * Default template for wrapping bar results - includes count of votes.
 *
 * Variables available:
 * - $nid: The nid of the poll
 * - $cancel_form: Provides a form for deleting user's votes when they have 
 *   permission to do so.
 * - $bars: The output of all styled bars displaying votes.
 * - $total: Total number of votes.
 * - $voted: An array indicating which unique choice ids were selected by the user.
 *
 */
?>
<style>
    
.field-name-field-advpoll-intro-text, .field-name-title { display:none;} 
/*.page-market-updates #home-survey-content { display:none;} */
</style>
<div class="poll" id="advpoll-<?php print $nid; ?>">

    <?php //print $bars; ?>
<!--    <div class="total"><?php //print t('Total votes: @total', array('@total' => $total)); ?></div>-->
    
    
        
     <?php
     
        
     
         
            echo '<p>Thank you</p>';
            echo '<div id="home-survey-content">';
             if ($path=="market-updates") {
                 echo '<p>Thanks for taking part in our quick survey. We update this regularly, so please check back again soon to answer a new question and find out the full results of this survey.</p>';
            } else {
                echo '<p>Below are the results for last months survey:</p>';   
                echo $body;
            }
            echo '</div>';
           
      ?>   
     <br/><br/>
    
   
    
    

     

    <?php print $cancel_form; ?>
</div>
