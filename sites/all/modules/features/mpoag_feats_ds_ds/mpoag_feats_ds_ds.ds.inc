<?php
/**
 * @file
 * mpoag_feats_ds_ds.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mpoag_feats_ds_ds_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|casestudy|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'casestudy';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'back_to_case_studies' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|casestudy|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function mpoag_feats_ds_ds_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'back_to_case_studies';
  $ds_field->label = 'Back to Case Studies';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->properties = array(
    'code' => array(
      'value' => '<a href="/about-us/case-studies"><input type="button" value="Back to Case Studies" /></a>',
      'format' => 'ds_code',
    ),
    'use_token' => 0,
  );
  $export['back_to_case_studies'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mpoag_feats_ds_ds_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|advpoll|question_block';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'advpoll';
  $ds_layout->view_mode = 'question_block';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_advpoll_intro_text',
        2 => 'advpoll_choice',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_advpoll_intro_text' => 'ds_content',
      'advpoll_choice' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|advpoll|question_block'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|advpoll|results_block';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'advpoll';
  $ds_layout->view_mode = 'results_block';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'field_advpoll_results_sum',
        1 => 'field_advpoll_results_image',
      ),
    ),
    'fields' => array(
      'field_advpoll_results_sum' => 'ds_content',
      'field_advpoll_results_image' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|advpoll|results_block'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|casestudy|case_study_summary';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'casestudy';
  $ds_layout->view_mode = 'case_study_summary';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'field_case_teaser_intro',
        1 => 'field_case_solution',
      ),
    ),
    'fields' => array(
      'field_case_teaser_intro' => 'ds_content',
      'field_case_solution' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|casestudy|case_study_summary'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|casestudy|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'casestudy';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_case_image',
        2 => 'back_to_case_studies',
        3 => 'field_tax_regions',
        4 => 'field_usr_consultant',
        5 => 'field_case_scenario',
        6 => 'field_case_solution',
        7 => 'field_case_outcome',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_case_image' => 'ds_content',
      'back_to_case_studies' => 'ds_content',
      'field_tax_regions' => 'ds_content',
      'field_usr_consultant' => 'ds_content',
      'field_case_scenario' => 'ds_content',
      'field_case_solution' => 'ds_content',
      'field_case_outcome' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|casestudy|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|casestudy|testimonial';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'casestudy';
  $ds_layout->view_mode = 'testimonial';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'field_case_client_testimonial',
        1 => 'field_case_image_testimonial',
      ),
    ),
    'fields' => array(
      'field_case_client_testimonial' => 'ds_content',
      'field_case_image_testimonial' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|casestudy|testimonial'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|heroimage|banner_images';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'heroimage';
  $ds_layout->view_mode = 'banner_images';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'field_banner_image',
      ),
    ),
    'fields' => array(
      'field_banner_image' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|heroimage|banner_images'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|jobs|search_index';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'jobs';
  $ds_layout->view_mode = 'search_index';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_job_intro',
        2 => 'field_job_client_img',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_job_intro' => 'ds_content',
      'field_job_client_img' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|jobs|search_index'] = $ds_layout;

  return $export;
}
