<?php
/**
 * @file
 * mpoag_feats_boxes.box.inc
 */

/**
 * Implements hook_default_box().
 */
function mpoag_feats_boxes_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'box_latest_update';
  $box->plugin_key = 'simple';
  $box->title = 'Latest Updates';
  $box->description = 'Latest Updates';
  $box->options = array(
    'body' => array(
      'value' => '<p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. At vero eos et accusam et justo duo dolores et ea rebum. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['box_latest_update'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'featured_article';
  $box->plugin_key = 'simple';
  $box->title = 'Featured Box';
  $box->description = 'Featured Box';
  $box->options = array(
    'body' => array(
      'value' => ' <div style="overflow:scroll;width: 269px; height:240px;overflow-x:hidden;">
<div style="float: left; min-height: 20px;  width: 232px;;font-size:11px;margin-bottom:10px;"><div class="feature2">&nbsp;</div><div class="info">The latest on unconventional oil and gas developments... read it <a href="http://oilandgas.page.com/insights/unconventional-oil-and-gas-developments">here</a></div></div>
 <div style="float: left; min-height: 20px;  width: 232px;font-size:11px;margin-bottom:10px;"> <div class="feature3">&nbsp;</div><div class="info">Find out how to develop your career as a <a href="https://oilandgas.page.com/insights/how-be-subsea-engineer">subsea engineer</a></div></div>
  <div style="float: left; min-height: 20px;  width: 232px;font-size:11px;margin-bottom:10px;"><div class="feature1">&nbsp;</div><div class="info">For insight into how we’d approach your recruitment, look at our <a href="http://oilandgas.page.com/about-us/case-studies">case studies</a></div></div>
<div style="float: left; min-height: 20px;  width: 232px;font-size:11px;margin-bottom:10px;"><div class="feature2">&nbsp;</div><div class="info">Launch your career in oil & gas! Here\'s how <a href="https://oilandgas.page.com/insights/how-develop-career-oil-and-gas">VP Engineering</a> did it...</div> </div>
  <div style="float: left; min-height: 20px;  width: 232px;font-size:11px;margin-bottom:10px;"><div class="feature3">&nbsp;</div><div class="info">Catch Huw Rothwell discussing Houston’s job market at the <a href="http://oilandgas.page.com/events/british-energy-day">American Petroleum Institute</a> on 9/4/13</a></div> </div>
</div>
 ',
      'format' => 'ds_code',
    ),
    'additional_classes' => '',
  );
  $export['featured_article'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'feedback_box';
  $box->plugin_key = 'simple';
  $box->title = 'Feedback';
  $box->description = 'Feedback Box';
  $box->options = array(
    'body' => array(
      'value' => '<p>We’d like to hear from you.</p><p>Is there a trend impacting your market?<br />Do you have a question that needs answering? Would you like to share your experience of our services?</p><p>If so, please contact us with your feedback.</p><p><a href="mailto:oilandgasfeedback@michaelpage.com.br?subject=Feedback%20from%20www.oilandgas.page.com"><input type="button" value="Submit Feedback" /></a></p>',
      'format' => 'ds_code',
    ),
    'additional_classes' => '',
  );
  $export['feedback_box'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'find_an_office';
  $box->plugin_key = 'simple';
  $box->title = 'Find an Office';
  $box->description = 'Find an Office';
  $box->options = array(
    'body' => array(
      'value' => '<p><img alt="" src="<?php global $base_path, $base_url; print $base_url; ?>/sites/all/themes/mp/css/images/sidebar_backs/SDB_office_map.jpg" style="width: 242px; height: 162px;" /></p><p><a href="/get-in-touch" title="Find an office" class="get-in-touch"><input type="button" value="Find an Office" /></a></p>',
      'format' => 'ds_code',
    ),
    'additional_classes' => '',
  );
  $export['find_an_office'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'market_updates_header';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Header image for market updates';
  $box->options = array(
    'body' => array(
      'value' => '<p><img alt="" src="<?php global $base_path, $base_url; print $base_url; ?>/sites/all/themes/mp/css/images/market_updates/MARKET_updates_bg_top.jpg" style="width: 610px; height: 172px;" /></p>',
      'format' => 'ds_code',
    ),
    'additional_classes' => '',
  );
  $export['market_updates_header'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'upload_cv';
  $box->plugin_key = 'simple';
  $box->title = 'Upload your CV';
  $box->description = 'Upload CV';
  $box->options = array(
    'body' => array(
      'value' => '<p><span style="line-height: 1.538em;">Want to be considered for oil and gas jobs across our global network? Send us a copy of your CV and specify your locations of interest.</span></p><p><span style="line-height: 1.538em;">We&rsquo;ll be in touch to discuss career opportunities soon.&nbsp;</span></p><p><a href="/find-job/cv-upload" title="Upload Your CV"><input type="button" value="Upload Your CV" /></a></p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['upload_cv'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'upload_cv_landing';
  $box->plugin_key = 'simple';
  $box->title = 'Upload CV Landing page';
  $box->description = 'Upload CV on landing page';
  $box->options = array(
    'body' => array(
      'value' => '<p><span style="line-height: 1.538em;">Want to consider international locations in your search for a new opportunity? Or would you prefer a new role close to home? At Michael Page Oil &amp; Gas we facilitate career moves for our candidates, globally.</span></p><p><span style="line-height: 1.538em;">Please send us a copy of your CV and specify your sectors and locations of interest.&nbsp;</span><span style="line-height: 1.538em;">We will be in touch to discuss relevant career opportunities for you soon.</span></p><p><a href="/find-job/cv-upload" title="Upload Your CV"><input type="button" value="Upload Your CV" /></a></p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['upload_cv_landing'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'upload_job_landing';
  $box->plugin_key = 'simple';
  $box->title = 'Upload Spec Landing page';
  $box->description = 'Upload Job Specification on landing page';
  $box->options = array(
    'body' => array(
      'value' => '<p><span style="line-height: 1.538em;">Identifying, attracting and retaining the right employees can be a challenge for oil and gas employers in a market that is often short-skilled. At Michael Page Oil &amp; Gas we recognise this and use our integrated global network to ensure access to the widest range of candidates for organisations wanting to hire.</span></p><p>Contact us to find how we could make a difference to your recruitment process.&nbsp;</p><p><a href="/job-hire/job-upload" title="Upload Upload Job Specification"><input type="button" value="Upload Your Job Spec" /></a></p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['upload_job_landing'] = $box;

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'upload_spec';
  $box->plugin_key = 'simple';
  $box->title = 'Upload Job Specification';
  $box->description = 'Upload Job Specification';
  $box->options = array(
    'body' => array(
      'value' => '<p><span style="line-height: 1.538em;">Contact Michael Page Oil &amp; Gas to find how our global network and track record can make a difference to your recruitment process.</span></p><p>Choose either your location or where the roles will be based, share your details and we&rsquo;ll be in touch to discuss your requirements.&nbsp;</p><p><a href="/job-hire/job-upload" title="Upload Job Specification"><input type="button" value="Upload a Job Specification" /></a></p>',
      'format' => 'full_html',
    ),
    'additional_classes' => '',
  );
  $export['upload_spec'] = $box;

  return $export;
}
