<?php
/**
 * @file
 * mpoag_feats_ds_vm.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function mpoag_feats_ds_vm_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'banner_images';
  $ds_view_mode->label = 'Banner Images';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['banner_images'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'case_study_summary';
  $ds_view_mode->label = 'Case Study Summary';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['case_study_summary'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'question_block';
  $ds_view_mode->label = 'Question Block';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['question_block'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'results_block';
  $ds_view_mode->label = 'Results Block';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['results_block'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'testimonial';
  $ds_view_mode->label = 'Testimonial';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['testimonial'] = $ds_view_mode;

  return $export;
}
