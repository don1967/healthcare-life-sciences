<?php
/**
 * @file
 * mpoag_feats_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mpoag_feats_views_views_api() {
  return array("version" => "3.0");
}
