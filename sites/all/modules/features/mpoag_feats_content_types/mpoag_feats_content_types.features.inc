<?php
/**
 * @file
 * mpoag_feats_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpoag_feats_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function mpoag_feats_content_types_node_info() {
  $items = array(
    'advpoll' => array(
      'name' => t('Survey'),
      'base' => 'node_content',
      'description' => t('Advanced Poll adds additional poll functionality, cookie voting, write-ins and voting modes.'),
      'has_title' => '1',
      'title_label' => t('Question'),
      'help' => '',
    ),
    'casestudy' => array(
      'name' => t('Case Study'),
      'base' => 'node_content',
      'description' => t('Use <em>case studies</em> for case studies and testimonials.'),
      'has_title' => '1',
      'title_label' => t('Company Name'),
      'help' => '',
    ),
    'events' => array(
      'name' => t('Events'),
      'base' => 'node_content',
      'description' => t('Use <em>events</em> for events for your Market Updates section.'),
      'has_title' => '1',
      'title_label' => t('Event Name'),
      'help' => '',
    ),
    'front_page_images' => array(
      'name' => t('Front Page Images'),
      'base' => 'node_content',
      'description' => t('<em>Front page images</em> are used for the scrolling image on the front page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'heroimage' => array(
      'name' => t('Banner Image'),
      'base' => 'node_content',
      'description' => t('<em>banner images</em> are used for the banner image used on multiple pages throughout the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'insights' => array(
      'name' => t('Insights'),
      'base' => 'node_content',
      'description' => t('Use <em>insights</em> for insight articles for your Market Updates section.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'jobs' => array(
      'name' => t('Job'),
      'base' => 'node_content',
      'description' => t('Use <em>job specifications</em> for jobs submitted via the Upload job spec form.'),
      'has_title' => '1',
      'title_label' => t('Job Title'),
      'help' => '',
    ),
    'location' => array(
      'name' => t('Office Location'),
      'base' => 'node_content',
      'description' => t('Use <em>office details</em> for contact details for an office.'),
      'has_title' => '1',
      'title_label' => t('Office Name'),
      'help' => '',
    ),
    'news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => t('Use <em>news</em> for news articles for your Market Updates section.'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
    'office_country' => array(
      'name' => t('Office Country'),
      'base' => 'node_content',
      'description' => t('Country locations for offices'),
      'has_title' => '1',
      'title_label' => t('Country Name'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'thank_you_page' => array(
      'name' => t('Thank You Page'),
      'base' => 'node_content',
      'description' => t('Thank you pages for forms'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
