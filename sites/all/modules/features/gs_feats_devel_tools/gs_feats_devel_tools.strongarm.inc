<?php
/**
 * @file
 * gs_feats_devel_tools.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gs_feats_devel_tools_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'dev_query';
  $strongarm->value = 0;
  $export['dev_query'] = $strongarm;

  return $export;
}
