<?php
/**
 * @file
 * mpoag_feats_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function mpoag_feats_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-menu-footer-center.
  $menus['menu-menu-footer-center'] = array(
    'menu_name' => 'menu-menu-footer-center',
    'title' => 'About us',
    'description' => '',
  );
  // Exported menu: menu-menu-footer-left.
  $menus['menu-menu-footer-left'] = array(
    'menu_name' => 'menu-menu-footer-left',
    'title' => 'Legal information',
    'description' => '',
  );
  // Exported menu: menu-menu-footer-right.
  $menus['menu-menu-footer-right'] = array(
    'menu_name' => 'menu-menu-footer-right',
    'title' => 'Menu Footer Right',
    'description' => '',
  );
  // Exported menu: menu-mpoag-menu-case-studies.
  $menus['menu-mpoag-menu-case-studies'] = array(
    'menu_name' => 'menu-mpoag-menu-case-studies',
    'title' => 'Case Study sub menu',
    'description' => 'Used on the Case Studies pages with links to About Us and Case Studies "home" page',
  );
  // Exported menu: menu-office-sub-menu.
  $menus['menu-office-sub-menu'] = array(
    'menu_name' => 'menu-office-sub-menu',
    'title' => 'Office Sub Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About us');
  t('Case Study sub menu');
  t('Legal information');
  t('Main menu');
  t('Menu Footer Right');
  t('Office Sub Menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('Used on the Case Studies pages with links to About Us and Case Studies "home" page');


  return $menus;
}
