<?php
/**
 * @file
 * mpoag_feats_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function mpoag_feats_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:<front>
  $menu_links['main-menu:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: main-menu:get-in-touch
  $menu_links['main-menu:get-in-touch'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'get-in-touch',
    'router_path' => 'get-in-touch',
    'link_title' => 'Get in touch',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
  );
  // Exported menu link: main-menu:looking-job
  $menu_links['main-menu:looking-job'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'looking-job',
    'router_path' => 'looking-job',
    'link_title' => 'Looking for a job?',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: main-menu:market-updates
  $menu_links['main-menu:market-updates'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'market-updates',
    'router_path' => 'market-updates',
    'link_title' => 'Market Updates',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Exported menu link: main-menu:node/1
  $menu_links['main-menu:node/1'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'About Us',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: main-menu:node/2
  $menu_links['main-menu:node/2'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'Looking to hire?',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Exported menu link: menu-menu-footer-center:http://www.page.com/
  $menu_links['menu-menu-footer-center:http://www.page.com/'] = array(
    'menu_name' => 'menu-menu-footer-center',
    'link_path' => 'http://www.page.com/',
    'router_path' => '',
    'link_title' => 'PageGroup – corporate',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-mpoag-menu-case-studies:about-us/case-studies
  $menu_links['menu-mpoag-menu-case-studies:about-us/case-studies'] = array(
    'menu_name' => 'menu-mpoag-menu-case-studies',
    'link_path' => 'about-us/case-studies',
    'router_path' => 'about-us/case-studies',
    'link_title' => 'Case Studies and Testimonials',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '5',
  );
  // Exported menu link: menu-mpoag-menu-case-studies:node/1
  $menu_links['menu-mpoag-menu-case-studies:node/1'] = array(
    'menu_name' => 'menu-mpoag-menu-case-studies',
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'How We Work',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About Us');
  t('Case Studies and Testimonials');
  t('Get in touch');
  t('Home');
  t('How We Work');
  t('Looking for a job?');
  t('Looking to hire?');
  t('Market Updates');
  t('PageGroup – corporate');


  return $menu_links;
}
