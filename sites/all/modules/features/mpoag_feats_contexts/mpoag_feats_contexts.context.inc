<?php
/**
 * @file
 * mpoag_feats_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function mpoag_feats_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about_us_section_blocks';
  $context->description = 'About us and Case Studies blocks';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-us' => 'about-us',
        'about-us/how-we-work' => 'about-us/how-we-work',
        'about-us/case-studies' => 'about-us/case-studies',
        'case-studies/*' => 'case-studies/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-mpoag-menu-case-studies' => array(
          'module' => 'menu',
          'delta' => 'menu-mpoag-menu-case-studies',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-survey_views-block_survey_curr' => array(
          'module' => 'views',
          'delta' => 'survey_views-block_survey_curr',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-survey_views-block_survey_prev' => array(
          'module' => 'views',
          'delta' => 'survey_views-block_survey_prev',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'boxes-find_an_office' => array(
          'module' => 'boxes',
          'delta' => 'find_an_office',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('About us and Case Studies blocks');
  t('Michael Page');
  $export['about_us_section_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'context_get_in_touch_landing';
  $context->description = 'Get in touch landing';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'get-in-touch' => 'get-in-touch',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-upload_job_landing' => array(
          'module' => 'boxes',
          'delta' => 'upload_job_landing',
          'region' => 'content',
          'weight' => '-31',
        ),
        'boxes-upload_cv_landing' => array(
          'module' => 'boxes',
          'delta' => 'upload_cv_landing',
          'region' => 'content',
          'weight' => '-30',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Get in touch landing');
  t('Michael Page');
  $export['context_get_in_touch_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cv_upload_landing';
  $context->description = 'User upload cv landing page';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'find-job/cv-upload' => 'find-job/cv-upload',
        'job-hire/job-upload' => 'job-hire/job-upload',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-find_an_office' => array(
          'module' => 'boxes',
          'delta' => 'find_an_office',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Michael Page');
  t('User upload cv landing page');
  $export['cv_upload_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'find_an_office';
  $context->description = 'Find an Office Block';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'get-in-touch' => 'get-in-touch',
        'office/*' => 'office/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mp_findoffice-mp_findoffice_findoffice' => array(
          'module' => 'mp_findoffice',
          'delta' => 'mp_findoffice_findoffice',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Find an Office Block');
  t('Michael Page');
  $export['find_an_office'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'get_in_touch_filter';
  $context->description = 'Adds exposed filter for NEI view';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'market-updates/all' => 'market-updates/all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-market_updates_header' => array(
          'module' => 'boxes',
          'delta' => 'market_updates_header',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views--exp-views_nei-page_nei_all' => array(
          'module' => 'views',
          'delta' => '-exp-views_nei-page_nei_all',
          'region' => 'content',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds exposed filter for NEI view');
  t('Michael Page');
  $export['get_in_touch_filter'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'hire_job_search_blocks';
  $context->description = 'Looking to hire and job search pages (Not landing)';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'job-search/results' => 'job-search/results',
        'jobs/*' => 'jobs/*',
        'job/specification-upload' => 'job/specification-upload',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-find_an_office' => array(
          'module' => 'boxes',
          'delta' => 'find_an_office',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Looking to hire and job search pages (Not landing)');
  t('Michael Page');
  $export['hire_job_search_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'job_details';
  $context->description = 'Job Details blocks';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~jobs/thank-you' => '~jobs/thank-you',
        'jobs/*' => 'jobs/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mp_submit_cv-mp_submit_cv_job_apply' => array(
          'module' => 'mp_submit_cv',
          'delta' => 'mp_submit_cv_job_apply',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Job Details blocks');
  t('Michael Page');
  $export['job_details'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'job_quick_search';
  $context->description = 'Quick Search Form';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-us/*' => 'about-us/*',
        'case-studies/*' => 'case-studies/*',
        'market-updates' => 'market-updates',
        'market-updates/*' => 'market-updates/*',
        'events/*' => 'events/*',
        'news/*' => 'news/*',
        'insights/*' => 'insights/*',
        'get-in-touch' => 'get-in-touch',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mp_searchform-mp_searchform_quicksearch' => array(
          'module' => 'mp_searchform',
          'delta' => 'mp_searchform_quicksearch',
          'region' => 'menu',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Michael Page');
  t('Quick Search Form');
  $export['job_quick_search'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'job_search_page';
  $context->description = 'Job Search Page block';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'search/job' => 'search/job',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-844499ae658959180c5ff1590ee517f4' => array(
          'module' => 'views',
          'delta' => '844499ae658959180c5ff1590ee517f4',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Job Search Page block');
  t('Michael Page');
  $export['job_search_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'looking_for_a_job_landing';
  $context->description = 'Looking for a job landing page';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'looking-job' => 'looking-job',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-d9ac51c6b64f330bc3b7b6ad2f5432f9' => array(
          'module' => 'views',
          'delta' => 'd9ac51c6b64f330bc3b7b6ad2f5432f9',
          'region' => 'content',
          'weight' => '-10',
        ),
        'boxes-upload_cv' => array(
          'module' => 'boxes',
          'delta' => 'upload_cv',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Looking for a job landing page');
  t('Michael Page');
  $export['looking_for_a_job_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'looking_to_hire_landing';
  $context->description = 'Landing page for looking to hire';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'looking-to-hire' => 'looking-to-hire',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mp_findoffice-mp_findoffice_findoffice' => array(
          'module' => 'mp_findoffice',
          'delta' => 'mp_findoffice_findoffice',
          'region' => 'content',
          'weight' => '10',
        ),
        'boxes-upload_spec' => array(
          'module' => 'boxes',
          'delta' => 'upload_spec',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Landing page for looking to hire');
  t('Michael Page');
  $export['looking_to_hire_landing'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'market_updates_landing_blocks';
  $context->description = 'Landing page for market-updates';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'market-updates' => 'market-updates',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-survey_views-block_survey_curr' => array(
          'module' => 'views',
          'delta' => 'survey_views-block_survey_curr',
          'region' => 'content',
          'weight' => '10',
        ),
        'boxes-feedback_box' => array(
          'module' => 'boxes',
          'delta' => 'feedback_box',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Landing page for market-updates');
  t('Michael Page');
  $export['market_updates_landing_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'market_updates_section_blocks';
  $context->description = 'Market Updates Blocks and Boxes';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'market-updates/*' => 'market-updates/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-feedback_box' => array(
          'module' => 'boxes',
          'delta' => 'feedback_box',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Market Updates Blocks and Boxes');
  t('Michael Page');
  $export['market_updates_section_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'michael_page_blocks';
  $context->description = 'Blocks for Michael Page';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-7',
        ),
        'delta_blocks-branding' => array(
          'module' => 'delta_blocks',
          'delta' => 'branding',
          'region' => 'branding',
          'weight' => '-10',
        ),
        'delta_blocks-messages' => array(
          'module' => 'delta_blocks',
          'delta' => 'messages',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks for Michael Page');
  t('Michael Page');
  $export['michael_page_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'michael_page_case_studies';
  $context->description = 'Case Studies and Testimonials';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-us/case-studies' => 'about-us/case-studies',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-8f9db864de12b1dd0a96161cac5bdadf' => array(
          'module' => 'views',
          'delta' => '8f9db864de12b1dd0a96161cac5bdadf',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Case Studies and Testimonials');
  t('Michael Page');
  $export['michael_page_case_studies'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'michael_page_front';
  $context->description = 'Front Page blocks';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mp_searchform-mp_searchform_advancedsearch' => array(
          'module' => 'mp_searchform',
          'delta' => 'mp_searchform_advancedsearch',
          'region' => 'content',
          'weight' => '30',
        ),
        'views-survey_views-block_survey_curr' => array(
          'module' => 'views',
          'delta' => 'survey_views-block_survey_curr',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-survey_views-block_survey_prev' => array(
          'module' => 'views',
          'delta' => 'survey_views-block_survey_prev',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'boxes-featured_article' => array(
          'module' => 'boxes',
          'delta' => 'featured_article',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Front Page blocks');
  t('Michael Page');
  $export['michael_page_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'michael_page_menu';
  $context->description = 'Global Context (Site wide for now)';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mp_logo_block-mp_logo_logo' => array(
          'module' => 'mp_logo_block',
          'delta' => 'mp_logo_logo',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'menu-menu-menu-footer-left' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-footer-left',
          'region' => 'footer_first',
          'weight' => '-9',
        ),
        'menu-menu-menu-footer-center' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-footer-center',
          'region' => 'footer_first',
          'weight' => '-8',
        ),
        'menu-menu-menu-footer-right' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-footer-right',
          'region' => 'footer_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Global Context (Site wide for now)');
  t('Michael Page');
  $export['michael_page_menu'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'thank_you_pages';
  $context->description = 'Thank you pages';
  $context->tag = 'Michael Page';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'thank_you_page' => 'thank_you_page',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-feedback_box' => array(
          'module' => 'boxes',
          'delta' => 'feedback_box',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'boxes-box_latest_update' => array(
          'module' => 'boxes',
          'delta' => 'box_latest_update',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Michael Page');
  t('Thank you pages');
  $export['thank_you_pages'] = $context;

  return $export;
}
