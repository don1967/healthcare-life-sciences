<?php
/**
 * @file
 * mpoag_feats_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function mpoag_feats_rules_default_rules_configuration() {
  $items = array();
  $items['rules_job_set_author_to_be_consultant'] = entity_import('rules_config', '{ "rules_job_set_author_to_be_consultant" : {
      "LABEL" : "Job Set Author to be consultant",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Job" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "jobs" : "jobs" } } } },
        { "NOT data_is" : { "data" : [ "node:field-usr-consultant" ], "value" : [ "node:author" ] } },
        { "NOT data_is" : { "data" : [ "node:author" ], "value" : "1" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-usr-consultant" ], "value" : [ "node:author" ] } }
      ]
    }
  }');
  $items['rules_job_set_closed'] = entity_import('rules_config', '{ "rules_job_set_closed" : {
      "LABEL" : "Close jobs older than 28 days",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Job" ],
      "REQUIRES" : [ "views_bulk_operations", "rules" ],
      "ON" : [ "cron" ],
      "IF" : [
        { "views_bulk_operations_condition_result_count" : { "view" : "jobs_older_than_28_days|page", "minimum" : "1" } }
      ],
      "DO" : [
        { "views_bulk_operations_action_load_list" : {
            "USING" : { "view" : "jobs_older_than_28_days|page" },
            "PROVIDE" : { "entity_list" : { "nodes_jobs" : "Jobs to be closed" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "nodes-jobs" ] },
            "ITEM" : { "nid" : "Current list item" },
            "DO" : [ { "component_rules_set_closed" : { "node" : [ "nid" ] } } ]
          }
        }
      ]
    }
  }');
  $items['rules_job_set_phone_number'] = entity_import('rules_config', '{ "rules_job_set_phone_number" : {
      "LABEL" : "Job Set Phone Number",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Job" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "jobs" : "jobs" } } } },
        { "data_is_empty" : { "data" : [ "node:field-usr-number" ] } }
      ],
      "DO" : [
        { "entity_fetch" : {
            "USING" : { "type" : "user", "id" : [ "node:field-usr-consultant:uid" ] },
            "PROVIDE" : { "entity_fetched" : { "user_cons" : "User Entity (Consultant)" } }
          }
        },
        { "data_set" : {
            "data" : [ "node:field-usr-number" ],
            "value" : [ "user-cons:field-usr-number" ]
          }
        }
      ]
    }
  }');
  $items['rules_mpoag_job_set_usd_salary'] = entity_import('rules_config', '{ "rules_mpoag_job_set_usd_salary" : {
      "LABEL" : "Set USD salary",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Job" ],
      "REQUIRES" : [ "rules", "php" ],
      "ON" : [ "node_presave" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "jobs" : "jobs" } } } }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : {
              "type" : "decimal",
              "value" : {
                "select" : "node:field-job-local-curr-code:tid",
                "php" : { "code" : "$curr = taxonomy_term_load($value);\\r\\n\\r\\n$currency = $curr-\\u003Efield_tax_cur_code[\\u0027und\\u0027][0][\\u0027safe_value\\u0027];\\r\\n$rates = db_select(\\u0027currency_exchange_rate_db_table\\u0027,\\u0027c\\u0027)\\r\\n-\\u003Efields(\\u0027c\\u0027,array(\\u0027rate\\u0027))\\r\\n-\\u003Econdition(\\u0027currency_code_from\\u0027, $currency,\\u0027=\\u0027)\\r\\n-\\u003Eexecute()\\r\\n-\\u003EfetchAssoc();\\r\\n$rate = (float)$rates[\\u0027rate\\u0027];\\r\\nreturn $rate;" }
              }
            },
            "PROVIDE" : { "variable_added" : { "rate" : "Exchange Rate" } }
          }
        },
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "node:field-job-loc-sal" ],
              "op" : "*",
              "input_2" : [ "rate" ]
            },
            "PROVIDE" : { "result" : { "dollar_rate" : "Converted Currency" } }
          }
        },
        { "data_convert" : {
            "USING" : { "type" : "integer", "value" : [ "dollar-rate" ] },
            "PROVIDE" : { "conversion_result" : { "dollar_int" : "Dollar Integar" } }
          }
        },
        { "data_set" : { "data" : [ "node:field-job-usd-sal" ], "value" : [ "dollar-int" ] } }
      ]
    }
  }');
  $items['rules_send_email_for_jobs_older_than_23_days'] = entity_import('rules_config', '{ "rules_send_email_for_jobs_older_than_23_days" : {
      "LABEL" : "Send email for jobs older than 23 days",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Job" ],
      "REQUIRES" : [ "views_bulk_operations", "rules" ],
      "ON" : [ "cron" ],
      "IF" : [
        { "views_bulk_operations_condition_result_count" : { "view" : "jobs_older_than_28_days|page", "minimum" : "1" } }
      ],
      "DO" : [
        { "views_bulk_operations_action_load_list" : {
            "USING" : { "view" : "jobs_older_than_28_days|page_23days" },
            "PROVIDE" : { "entity_list" : { "nodes_jobs" : "Jobs to be emailed" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "nodes-jobs" ] },
            "ITEM" : { "nid" : "Current list item" },
            "DO" : [ { "component_rules_send_mail" : { "node" : [ "nid" ] } } ]
          }
        }
      ]
    }
  }');
  $items['rules_send_mail'] = entity_import('rules_config', '{ "rules_send_mail" : {
      "LABEL" : "Send Mail",
      "PLUGIN" : "rule set",
      "TAGS" : [ "Job" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "PROVIDE" : { "entity_fetched" : { "user_to_email" : "User Entity" } },
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "jobs" : "jobs" } } } }
            ],
            "DO" : [
              { "entity_fetch" : {
                  "USING" : { "type" : "user", "id" : [ "node:author:uid" ] },
                  "PROVIDE" : { "entity_fetched" : { "user_to_email" : "User Entity" } }
                }
              },
              { "mail" : {
                  "to" : "[user-to-email:mail]",
                  "subject" : "Your job on oilandgas.page.com will close in 5 days",
                  "message" : "Hello,\\r\\n\\r\\nOne of the jobs you have advertised on the Michael Page Oil \\u0026 Gas website will automatically close\\r\\nin five days time. Here are the details:\\r\\n\\r\\nJob title: [node:title]\\r\\nJob reference number: [node:field-job-ref-no]\\r\\n\\r\\nIf you would like to change this action please access the job by logging into your area on the site\\r\\nusing this link:\\r\\nhttps:\\/\\/oilandgas.page.com\\/user\\r\\n\\r\\nTo keep the job open for another 28 days: click edit and save\\r\\nTo close the job now: click edit, scroll to the bottom of the job upload form and click close and save\\r\\n\\r\\nIf you close the job, it will still be available for you to re-open at a later date.\\r\\n\\r\\nFor site support, please email:\\r\\noilandgasfeedback@michaelpage.com.br",
                  "from" : "oilandgasfeedback@michaelpage.com.br",
                  "language" : [ "" ]
                }
              }
            ],
            "LABEL" : "Send Mail"
          }
        }
      ]
    }
  }');
  $items['rules_set_closed'] = entity_import('rules_config', '{ "rules_set_closed" : {
      "LABEL" : "Set Closed",
      "PLUGIN" : "rule set",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "jobs" : "jobs" } } } }
            ],
            "DO" : [ { "data_set" : { "data" : [ "node:field-job-status" ], "value" : 0 } } ],
            "LABEL" : "Close the Job"
          }
        }
      ]
    }
  }');
  return $items;
}
