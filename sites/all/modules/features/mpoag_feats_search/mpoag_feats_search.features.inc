<?php
/**
 * @file
 * mpoag_feats_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpoag_feats_search_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_search_api_index().
 */
function mpoag_feats_search_default_search_api_index() {
  $items = array();
  $items['default_node_index'] = entity_import('search_api_index', '{
    "name" : "Job Search Index",
    "machine_name" : "default_node_index",
    "description" : "Tweaked index for job content type",
    "server" : "production",
    "item_type" : "node",
    "options" : {
      "index_directly" : 1,
      "cron_limit" : "100",
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-50",
          "settings" : { "default" : "0", "bundles" : { "jobs" : "jobs" } }
        },
        "search_api_alter_node_status" : { "status" : 1, "weight" : "-49", "settings" : [] },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "-48",
          "settings" : { "fields" : { "field_tax_regions:parent" : "field_tax_regions:parent" } }
        },
        "search_api_alter_node_access" : { "status" : 1, "weight" : "-47", "settings" : [] },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "-46", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "-45", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "-44",
          "settings" : { "fields" : { "search_api_aggregation_1" : {
                "name" : "Location-text",
                "type" : "fulltext",
                "fields" : [ "field_tax_regions", "field_job_location" ],
                "description" : "A Fulltext aggregation of the following fields: Region, Location."
              }
            }
          }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_job_loc_sal" : true,
              "field_job_ref_no" : true,
              "field_usr_number" : true,
              "body:value" : true,
              "field_usr_consultant:name" : true,
              "field_usr_consultant:mail" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 1,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_tax_regions" : true,
              "field_tax_subsector" : true,
              "field_job_loc_sal" : true,
              "field_job_ref_no" : true,
              "field_usr_number" : true,
              "body:value" : true,
              "field_usr_consultant:name" : true,
              "field_usr_consultant:mail" : true
            },
            "title" : 1,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 1,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_job_loc_sal" : true,
              "field_job_ref_no" : true,
              "field_usr_number" : true,
              "body:value" : true,
              "field_usr_consultant:name" : true,
              "field_usr_consultant:mail" : true
            },
            "spaces" : "[^\\\\p{L}\\\\p{N}]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_job_loc_sal" : true,
              "field_job_ref_no" : true,
              "field_usr_number" : true,
              "body:value" : true,
              "field_usr_consultant:name" : true,
              "field_usr_consultant:mail" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      },
      "fields" : {
        "type" : { "type" : "string" },
        "title" : { "type" : "text", "boost" : "5.0" },
        "promote" : { "type" : "boolean" },
        "sticky" : { "type" : "boolean" },
        "created" : { "type" : "date" },
        "changed" : { "type" : "date" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "comment_count" : { "type" : "integer" },
        "field_tax_regions" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_usr_consultant" : { "type" : "integer", "entity_type" : "user" },
        "field_tax_subsector" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_job_contract_perm" : { "type" : "string" },
        "field_job_hide_sal" : { "type" : "boolean" },
        "field_job_ref_no" : { "type" : "text" },
        "field_job_featured" : { "type" : "boolean" },
        "field_job_updated" : { "type" : "date" },
        "field_usr_number" : { "type" : "text" },
        "field_job_status" : { "type" : "boolean" },
        "field_job_location" : { "type" : "text" },
        "field_job_loc_sal" : { "type" : "integer" },
        "field_job_usd_sal" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "search_api_aggregation_1" : { "type" : "text" },
        "body:value" : { "type" : "text" },
        "field_usr_consultant:name" : { "type" : "text" },
        "field_usr_consultant:mail" : { "type" : "text" }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function mpoag_feats_search_default_search_api_server() {
  $items = array();
  $items['development'] = entity_import('search_api_server', '{
    "name" : "Development",
    "machine_name" : "development",
    "description" : "Development Solr Server",
    "class" : "search_api_solr_service",
    "options" : {
      "host" : "95.138.174.112",
      "port" : "8080",
      "path" : "\\/solr",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "0",
    "rdf_mapping" : []
  }');
  $items['production'] = entity_import('search_api_server', '{
    "name" : "Production",
    "machine_name" : "production",
    "description" : "Production",
    "class" : "search_api_solr_service",
    "options" : {
      "host" : "oilandgas.page.com",
      "port" : "8443",
      "path" : "\\/solr",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
