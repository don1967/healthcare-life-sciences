<?php

/**
 * @file
 *   Populate distribution with some content.
 */

/**
 * Common Features
 */
abstract class MPMigration extends Migration {

  public function __construct() {
    // Always call the parent constructor first for basic setup
    parent::__construct();
  }

  public function processImport(array $options = array()) {
    parent::processImport($options = array());
    // Do not force menu rebuilding. Otherwise pathauto will try to rebuild
    // in each node_insert invocation.
    variable_set('menu_rebuild_needed', FALSE);
  }
}

class regionsMigration extends Migration {

  public function __construct() {
    parent::__construct();

    $this->description = t('Migrates in taxonomy terms for the Regions taxonomy');
    $this->map = new MigrateSQLMap($this->machineName,
      array('name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    //Useful "auto" source mechanism
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'mp_migrate') . '/files/region.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('regions');

    //Format for mapping $this->addFieldMapping(TO,FROM)
    $this->addSimpleMappings(array('name','weight','parent_name'));
    $this->addFieldMapping('format')->defaultValue('plain_text');

    $this->addFieldMapping('description')
    ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
    ->issueGroup(t('DNM'));

    if(module_exists('path')) {
      $this->addFieldMapping('path')
      ->issueGroup('DNM');
      if(module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
        ->issueGroup('DNM');
      }
    }
  } #END __construct

  function csvcolumns() {
    //"Age Range","Min Age","Max Age"
    $columns[0] = array('name', 'Region');
    $columns[1] = array('parent_name', 'Parent');
    $columns[3] = array('weight', 'Display Order');

    return $columns;
  } #END csvcolummns

  public function processImport(array $options = array()) {
    parent::processImport($options = array());
    // Do not force menu rebuilding. Otherwise pathauto will try to rebuild
    // in each node_insert invocation.
    variable_set('menu_rebuild_needed', FALSE);
  }#END processImport
} #END Class

class subsectorsMigration extends Migration {

  public function __construct() {
    parent::__construct();

    $this->description = t('Migrates in taxonomy terms for the subsectors taxonomy');
    $this->map = new MigrateSQLMap($this->machineName,
      array('name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    //Useful "auto" source mechanism
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'mp_migrate') . '/files/subsector.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('subsectors');

    //Format for mapping $this->addFieldMapping(TO,FROM)
    $this->addSimpleMappings(array('name','weight'));
    $this->addFieldMapping('format')->defaultValue('plain_text');

    $this->addFieldMapping('description')
    ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
    ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent_name')
    ->issueGroup(t('DNM'));

    if(module_exists('path')) {
      $this->addFieldMapping('path')
      ->issueGroup('DNM');
      if(module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
        ->issueGroup('DNM');
      }
    }
  } #END __construct

  function csvcolumns() {
    //"Age Range","Min Age","Max Age"
    $columns[0] = array('name', 'Subsector');
    $columns[3] = array('weight', 'Display Order');

    return $columns;
  } #END csvcolummns

  public function processImport(array $options = array()) {
    parent::processImport($options = array());
    // Do not force menu rebuilding. Otherwise pathauto will try to rebuild
    // in each node_insert invocation.
    variable_set('menu_rebuild_needed', FALSE);
  }#END processImport
} #END Class

class currenciesMigration extends Migration {

  public function __construct() {
    parent::__construct();

    $this->description = t('Migrates in taxonomy terms for the Currencies taxonomy');
    $this->map = new MigrateSQLMap($this->machineName,
      array('name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    //Useful "auto" source mechanism
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'mp_migrate') . '/files/currencies.csv', $this->csvcolumns(), array('header_rows' => 1));
    $this->destination = new MigrateDestinationTerm('currencies');

    //Format for mapping $this->addFieldMapping(TO,FROM)
    $this->addSimpleMappings(array('name','field_tax_cur_code'));
    $this->addFieldMapping('format')->defaultValue('plain_text');

    $this->addFieldMapping('description')
    ->issueGroup(t('DNM'));
    $this->addFieldMapping('parent')
    ->issueGroup(t('DNM'));

    if(module_exists('path')) {
      $this->addFieldMapping('path')
      ->issueGroup('DNM');
      if(module_exists('pathauto')) {
        $this->addFieldMapping('pathauto')
        ->issueGroup('DNM');
      }
    }
  } #END __construct

  function csvcolumns() {
    //"Code","Country Name"
    $columns[0] = array('field_tax_cur_code', 'Code');
    $columns[1] = array('name', 'Country Name');
    

    return $columns;
  } #END csvcolummns

  public function processImport(array $options = array()) {
    parent::processImport($options = array());
    // Do not force menu rebuilding. Otherwise pathauto will try to rebuild
    // in each node_insert invocation.
    variable_set('menu_rebuild_needed', FALSE);
  }#END processImport
} #END Class

class usersMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Import users');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'mail' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationUser::getKeySchema()
    );

    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/' . drupal_get_path('module', 'mp_migrate') . '/files/user.csv', $this->csvcolumns(), array('header_rows' => 1));

    $this->destination = new MigrateDestinationUser();

    $this->addSimpleMappings(array('name', 'mail', 'pass')); //My thanks to the documentation!
    $this->addFieldMapping('roles', 'roles')->separator('|');

    $this->addFieldMapping('is_new')->defaultValue(TRUE);
    $this->addFieldMapping('status')->defaultValue(1);
  }

  public function prepare(stdClass $user, stdClass $source_row) {
    $role_names = $user->roles;
    $roles = array();

    foreach ($role_names as $role_name) {
      if ($role = user_role_load_by_name($role_name)) {
        $roles[$role->rid] = $role->rid;
      }
    }

    $user->roles = $roles;
  }

  function csvcolumns() {
    // "Name","Mail","Pass","Roles"
    $columns[0] = array('name', 'Name');
    $columns[1] = array('mail', 'Mail');
    $columns[2] = array('pass', 'Pass');
    $columns[3] = array('roles', 'Roles');
    return $columns;
  }

  public function processImport(array $options = array()) {
    parent::processImport($options = array());
    // Do not force menu rebuilding. Otherwise pathauto will try to rebuild
    // in each node_insert invocation.
    variable_set('menu_rebuild_needed', FALSE);
  }
}