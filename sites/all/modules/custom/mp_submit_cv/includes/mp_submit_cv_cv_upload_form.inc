<?php

/**
 * Implements hook_form()
 */
function mp_submit_cv_csu_form($form, &$form_state) {
  $nid_arr = array();
  $nid_arr = explode('/',check_plain($_GET['q']));

  $node = node_load((int) $nid_arr[1]);
  $nid_arr = null;
  
  /*if(empty($node->field_job_status['und']['0']['value'])) {
    return FALSE;
  }*/
  //Work out the back office
  
  //Get today's dates (Server time will suffice as it's highly unlikely someone 
  //born "today" will apply for a job)
  $today = date('d-m-Y');

  //shown elements
  $form['csu_first_name'] = array(
    '#title' => t('First Name'),
    '#type' => 'textfield',
    '#default_value' => t('First Name'),
  );

  $form['csu_last_name'] = array(
    '#title' => t('Last Name'),
    '#type' => 'textfield',
    '#default_value' => t('Last Name'),
  );

  $form['csu_full_name'] = array(
    '#title' => t('Full Name'),
    '#type' => 'textfield',
    '#default_value' => t('Full Name'),
  );

  $form['csu_email_address'] = array(
    '#title' => t('Email Address'),
    '#type' => 'textfield',
    '#default_value' => t('Email'),
  );

  $form['csu_upload'] = array(
    '#name' => 'files[csu_upload_1]',
    '#type' => 'file',
    '#title' => t('Upload your CV'),
    '#description' => 'Identify the location of your CV (.doc, .docx, .pdf, .rtf, .txt, .html) size of 2MB',
    '#size' => 22,
  );

  $form['csu_regions'] = array(
    '#type' => 'select',
    '#options' => _get_vocab_from_name('regions','All',0,1),
  );

  $form['csu_subsectors'] = array(
    '#type' => 'select',
    '#options' => _get_vocab_from_name('subsectors'),
  );

  $form['csu_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date of birth'),
    '#description' => t('Date of Birth (dd-mm-yyyy) : '),
    '#date_format' => 'd-m-Y',
    '#date_year_range' => '1900:0',
    '#datepicker_options' => array(
      'minDate' => '01-01-1900',
      'maxDate' => $today,
    ),
  );

  $form['csu_available_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Available from'),
    '#description' => t('Date available from (dd-mm-yyyy) : '),
    '#date_format' => 'd-m-Y',
    '#date_year_range' => '1900:0',
    '#datepicker_options' => array(
      'minDate' => '01-01-1900',
      'maxDate' => $today,
    ),
  );

  $form['csu_comments'] = array(
    '#title' => t('Comments'),
    '#type' => 'textarea',
    '#resizable' => FALSE,
    '#description' => t(''),
    '#default_value' => t('Comments'),
  );
  
  $form['csu_current'] = array(
    '#title' => t('Current job title'),
    '#type' => 'textfield',
    '#default_value' => t('Current job title'),
  );

  $form['csu_disclaimer'] = array(
    '#title' => t('Disclaimer'),
    '#type' => 'markup',
    '#markup' => '<div class="to-be-filled">Disclaimer</div>',
  );

  $form['csu_terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Terms &amp; Conditions check box'),
    '#default_value' => FALSE,
    //'#required' => TRUE,
  );

  $form['csu_cep'] = array(
    '#type' => 'textfield',
    '#title' => t('CEP'),
    '#default_value' => t('CEP'),
  );

  $form['csu_disabilities'] = array(
    '#type' => 'checkbox',
    '#title' => t('Person with Disabilities (PWD)?'),
    '#default_value' => FALSE,
  );

  $form['csu_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => t('City'),
  );

  $form['csu_specialty'] = array(
    '#type' => 'textfield',
    '#title' => t('Specialty'),
    '#default_value' => t('Specialty'),
  );

  $form['submit'] = array(
   '#type' => 'submit',
   '#value' => 'Submit CV',
  );

  return $form;
}

function mp_submit_cv_csu_form_validate($form, &$form_state) {

  $fileextensions = array('doc docx pdf rtf txt html');
  $filesize = array('2097152');

  $validators = array(
     'file_validate_size' => $filesize,
     'file_validate_extensions' => $fileextensions,
  );
  //save file as temporary
  $file = file_save_upload('csu_upload_1', $validators);
  
  if($file) {
    $file = file_move($file, 'public://'.strtr($file->filename,' ','_'));
    if($file) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t('Failed to write the uploaded file to the site\'s file folder.'));
    }
  }
  
  $form_state['storage']['file'] = $file;
 
}

function mp_submit_cv_csu_form_submit($form, &$form_state) {
  global $base_url;
  
  //Work out where to send the mail
  $host = preg_replace('#^https?://#', '', strtolower($_SERVER['SERVER_NAME'])); //strtolower($base_url));

  if($host == 'oilandgas.page.com') {
    $mode = 'live';
  }
  //Staging - DT and SSM
  elseif ($host == 'mpoag.golleyslaterdigital.co.uk') {
    $mode = 'staging';
  }
  //DT Dev
  elseif ($host == 'mpoag.cfcs.co.uk'){
    $mode = 'dt-dev';
  }
  //SSM Dev
  elseif ($host == 'localhost') {
    $mode = 'ssm-dev';
  }

  //Choose template based off country
  $mailArray = _create_cvupload_email_from_country($form_state['values'], $form_state['values']['csu_regions'], $mode);

  //Sort out email headers
  $headers = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8;',
    'Content-Transfer-Encoding' => 'quoted-printable',
    'X-Mailer' => 'Drupal',
    'reply-to' => $mailArray['reply-to'],
  );
    
  $file = $form_state['storage']['file'];
  
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['file']);
  $location = DRUPAL_ROOT.'/'.variable_get('file_public_path').'/'.$file->filename;
  
  //Work out where to send the mail
  switch($mode) {
    case 'live' :
      $to_address = $mailArray['to_address'].',lisakesson@michaelpage.com';
    
      //',jeremy@gatewayprogramme.com,lisakesson@michaelpage.com,venkatreddy@michaelpage.com,jeremygaywood@michaelpage.com,Paulopires@michaelpage.com.br';
    
      break;

    case 'staging':
      $blacklist = array('dthorne@contemporaryfusion.co.uk','stevemason@golleyslater.co.uk','ssmason@gmail.com','mtemple@golleyslater.co.uk',',mel_at_a_distance@hotmail.com');
      if(!in_array($form_state['values']['csu_email_address'], $blacklist)) {
        $to_address = $mailArray['to_address'];  
      } 
      else {
        $to_address = 'dthorne@contemporaryfusion.co.uk,stevemason@golleyslater.co.uk,mtemple@golleyslater.co.uk,mel_at_a_distance@hotmail.com';  
      }
      break;

    case 'dt-dev':
      $to_address = 'dthorne@contemporaryfusion.co.uk';//,stevemason@golleyslater.co.uk,mtemple@golleyslater.co.uk';
      break;

    case 'ssm-dev':
      $to_address = 'stevemason@golleyslater.co.uk';
      break;
  }

  //Send email
  drupal_mail_wrapper_attachment('mail',$to_address,$mailArray['subject'],nl2br($mailArray['body']),$mailArray['from_address'],$mailArray['from_name'],$headers,$mailArray['charset'],array($location));
  //Delete file
  file_delete($file);
  $form_state['redirect'] = array('find-job/thank-you');
}
