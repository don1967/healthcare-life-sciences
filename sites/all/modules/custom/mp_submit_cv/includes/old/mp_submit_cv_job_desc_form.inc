<?php

/**
 * Implements hook_form()
 */
 function mp_submit_cv_jsu_form($form, &$form_state) {
  
  $nid_arr = array();
  $nid_arr = explode('/',check_plain($_GET['q']));

  $node = node_load((int) $nid_arr[1]);
  $nid_arr = null;

  //shown elements
  $form['jsu_first_name'] = array(
    '#title' => t('First Name'),
    '#type' => 'textfield',
    '#default_value' => t('First Name'),
  );

  $form['jsu_last_name'] = array(
    '#title' => t('Last Name'),
    '#type' => 'textfield',
    '#default_value' => t('Last Name'),
  );

  $form['jsu_email_address'] = array(
    '#title' => t('Email Address'),
    '#type' => 'textfield',
    '#default_value' => t('Email'),
  );

  $form['jsu_upload'] = array(
    '#name' => 'files[jsu_upload_1]',
    '#type' => 'file',
    '#title' => t('Upload your job description'),
    '#description' => 'Identify the location of your job description (.doc, .docx, .pdf, or .rtf) size of 400 KBytes',
    '#size' => 22,
  );

  $form['jsu_regions'] = array(
    '#type' => 'select',
    '#options' => _get_countries('regions','All',0,12),
  );

  $form['jsu_comments'] = array(
    '#title' => t('Comments'),
    '#type' => 'textarea',
    '#resizable' => FALSE,
    '#description' => t(''),
    '#default_value' => t('Comments'),
  );
  
  $form['jsu_terms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Terms &amp; Conditions check box'),
    '#default_value' => FALSE,
    //'#required' => TRUE,
  );

  $form['submit'] = array(
   '#type' => 'submit',
   '#value' => 'Submit job specification',
  );
  return $form;
 }

function mp_submit_cv_jsu_form_validate($form, &$form_state) {
  
  $term = taxonomy_term_load($form_state['values']['jsu_regions']);
  $country = $term->name;
  //set country to jsu
  $form_state['values']['jsu_regions'] = $country;
  
  switch($country) {
    case 'Africa':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('512000');
      break;

    case 'Australia':
      $fileextensions = array('doc pdf txt');
      $filesize = array('307200');
      break;

    case 'Brazil':
      $filesize = array('512000');
      $fileextensions = array('doc pdf rtf');
      break;

    case 'Canada':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('307200');
      break;

    case 'Colombia':
      $fileextensions = array('doc txt rtf');
      $filesize = array('307200');
      break;

    case 'France':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('512000');
      break;

    case 'Italy':
      $fileextensions = array('doc pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Malaysia':
      $fileextensions = array('doc pdf rtf');
      $filesize = array('307200');
      break;

    case 'Middle East':
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'Netherlands':
      $fileextensions = array('doc pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Norway':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Portugal':
      $fileextensions = array('doc pdf rtf txt html');
      $filesize = array('512000');
      break;

    case 'Qatar':
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'Rest of Africa':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('512000');
      break;

    case 'Singapore':
      $fileextensions = array('doc pdf rtf');
      $filesize = array('307200');
      break;

    case 'Spain':
      $fileextensions = array('doc docx pdf rtf txt html');
      $filesize = array('512000');
      break;

    case 'South Africa':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('512000');
      break;

    case 'Turkey':
      $fileextensions = array('doc rtf txt');
      $filesize = array('307200');
      break;

    case 'UAE':
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'UK':
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'USA':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('307200');
      break;
  
    case 'New Zealand':
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('307200');
      break;

  }
  $validators = array(
     'file_validate_size' => $filesize,
     'file_validate_extensions' => $fileextensions,
  );
  //save file as temporary
  $file = file_save_upload('jsu_upload_1', $validators);
  
  if($file) {
    $file = file_move($file, 'public://'.strtr($file->filename,' ','_'));
    if($file) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t('Failed to write the uploaded file to the site\'s file folder.'));
    }
  }
  
  $form_state['storage']['file'] = $file;
}

function mp_submit_cv_jsu_form_submit($form, &$form_state) {
  global $base_url;
  
  //Work out where to send the mail
  $host = preg_replace('#^https?://#', '', strtolower($_SERVER['SERVER_NAME'])); //strtolower($base_url));

  if($host == 'oilandgas.page.com') {
    $mode = 'live';
  }
  //Staging - DT and SSM
  elseif ($host == 'mpoag.golleyslaterdigital.co.uk') {
    $mode = 'staging';
  }
  //DT Dev
  elseif ($host == 'mpoag.cfcs.co.uk'){
    $mode = 'dt-dev';
  }
  //SSM Dev
  elseif ($host == 'localhost') {
    $mode = 'ssm-dev';
  } 

  //Choose template based off country
  $mailArray = _create_job_desc_email_from_country($form_state['values'], $form_state['values']['jsu_regions'], $mode);

  //Sort out email headers
  $headers = array(
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8;',
    'Content-Transfer-Encoding' => 'quoted-printable',
    'X-Mailer' => 'Drupal',
    'reply-to' => $mailArray['reply-to'],
  );

  $file = $form_state['storage']['file'];
  
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['file']);
  $location = DRUPAL_ROOT.'/'.variable_get('file_public_path').'/'.$file->filename;
  
  //Work out where to send the mail
  switch($mode) {
    case 'live' :
      $to_address = $mailArray['to_address'].',lisakesson@michaelpage.com';
      break;

    case 'staging':
      $blacklist = array('dthorne@contemporaryfusion.co.uk','stevemason@golleyslater.co.uk','ssmason@gmail.com','mtemple@golleyslater.co.uk','mel_at_a_distance@hotmail.com');
      if(!in_array($form_state['values']['csu_email_address'], $blacklist)) {
        $to_address = $mailArray['to_address'];  
      } 
      else {
        $to_address = 'dthorne@contemporaryfusion.co.uk,stevemason@golleyslater.co.uk,mtemple@golleyslater.co.uk,mel_at_a_distance@hotmail.com';  
      }
      break;

    case 'dt-dev':
      $to_address = 'dthorne@contemporaryfusion.co.uk'; //,stevemason@golleyslater.co.uk,mtemple@golleyslater.co.uk';
      break;

    case 'ssm-dev':
      $to_address = 'stevemason@golleyslater.co.uk';
      break;
  }

  //Send email
  drupal_mail_wrapper_attachment('mail',$to_address,$mailArray['subject'],
    nl2br($mailArray['body']),$mailArray['from_address'],
    $mailArray['from_name'],$headers,$mailArray['charset'],array($location));
  //Delete file
  file_delete($file);
  $form_state['redirect'] = array('job-hire/thank-you');
}

function _get_countries($vocab='',$allText = 'All', $parents = 0, $max_depth = NULL) {
    
    
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocab);
  $tree = taxonomy_get_tree($vocabulary->vid, $parents, $max_depth);

  $returnTree = array();
  $returnTree[$allText] = t(ucfirst(substr($vocabulary->name,0,-1)));
  foreach ($tree as $terms => $term) {
   if($term->parents[0] != '0') {
      $treeToSort[] = $term->name.'-'.$term->tid;
    }
  }
  sort($treeToSort);
  foreach($treeToSort as $items) {
    $item = explode('-', $items);
    $treeToSort[$item[0]] = t($item[1]);
  }
  sort($treeToSort);
  foreach($treeToSort as $items) {
    $item = explode('-', $items);
    $returnTree[$item[1]] = t($item[0]);
  }
  return $returnTree;
}
