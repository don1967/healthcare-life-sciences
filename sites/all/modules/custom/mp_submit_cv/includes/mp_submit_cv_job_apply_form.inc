<?php

/**
 * The job application form 
 * @see mp_submit_cv_block_info
 */
function mp_submit_cv_job_apply_form($form, &$form_state) {
  //Load the node
  $nid_arr = array();
  $nid_arr = explode('/',check_plain($_GET['q']));

  $node = node_load((int) $nid_arr[1]);
  
    
  
  $nid_arr = null;

  if(!empty($_SESSION['search_param']) && $_SESSION['search_param'] != '?') {
    $searchString = $_SESSION['search_param'];
  }
  elseif(!empty($_SESSION['secure_search_param']) && $_SESSION['search_param'] != '?') {
    $searchString = $_SESSION['secure_search_param'];
  }
  elseif(!empty($_COOKIE['search_param']) && $_SESSION['search_param'] != '?') {
    $searchString = $_COOKIE['search_param'];
  }
  elseif(!empty($_COOKIE['secure_search_param']) && $_SESSION['search_param'] != '?') {
    $searchString = $_COOKIE['secure_search_param'];
  }
  else {
    $searchString = '?field_job_title=Job+title&field_job_location=Location&field_job_subsector=All&field_job_region=All&field_job_con_perm=All&field_job_min_sal=0&field_job_max_sal=1000000';
  }

  if($node->field_job_status['und']['0']['value'] == 0) {

    $form['ajf_closed'] = array(
      '#title' => t('Applications are closed'),
      '#type' => 'markup',
      '#markup' => 'Please note we are no longer accepting applications for this position <br />
      <a href="/looking-job'.$searchString.'">Back to search results</a>', 
    );
    return $form;
  }
  //Work out the back office
  if(!empty($node->field_job_back_office) || check_plain($node->field_tax_regions['und']['0']['taxonomy_term']->name) == 'Other') {
    $term = taxonomy_term_load($node->field_job_back_office['und']['0']['tid']);
    $backoffice = $term->name;
  }
  else {
    $backoffice = check_plain($node->field_tax_regions['und']['0']['taxonomy_term']->name); 
  }
  $job_ref = check_plain($node->field_job_ref_no['und']['0']['safe_value']);
  
  //Does Page Personel exist?
  if(!empty($node->field_job_page_personnel)) {
    $pp = check_plain($node->field_job_page_personnel['und']['0']['value']);
  }
  else {
    $pp = 0;
  }

  //Hidden elements (Shown for now)
  $form['afj_job_ref'] = array(
    '#type' => 'hidden',
    '#default_value' => $job_ref,
  );
  $form['afj_backoffice'] = array(
    '#type' => 'hidden',
    '#default_value' => $backoffice,
  );
  $form['afj_pp'] = array(
    '#type' => 'hidden',
    '#default_value' => $pp,
  );
  $form['afj_node'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->nid,
  );
  $form['afj_user'] = array(
    '#type' => 'hidden',
    '#default_value' => $node->field_usr_consultant['und']['0']['target_id'],
  );

  $form['afj_search'] = array(
    '#type' => 'hidden',
    '#default_value' => $searchString,
  );
  $node = null; //clear node for later

  //Conditional flags
  $comments = FALSE;
  $telephone = FALSE;
  $subsectors = FALSE;
  $regions = FALSE;
  $current = FALSE;
  $disclaimer = FALSE;
  $visa = FALSE;
  $dob = FALSE;
  $location = FALSE;
  $terms = FALSE;
  $jlt = FALSE;
  $addr = FALSE;
  $mobile = FALSE;
  $type = FALSE;
  $disclaimerText = '';
  $termsText = '';
  $path = '';

    
  switch($backoffice) {
      
    case 'Malaysia':
      $path = drupal_lookup_path('source','office/malaysia');
      $comments = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc docx pdf txt');
      $filesize = array('300');
      break;
  
      
      
      
    case 'Africa':
      $path = drupal_lookup_path('source','office/africa');
      $terms = TRUE;
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('512000');
      break;

    case 'Australia':
      $path = drupal_lookup_path('source','office/australia');
      $comments = TRUE;
      $terms=TRUE;
      $fileextensions = array('doc pdf txt');
      $filesize = array('307200');
      break;

   case 'New Zealand':
      $path = drupal_lookup_path('source','office/new-zealand');
      $comments = TRUE;
      $terms=TRUE;
      $fileextensions = array('doc docx pdf txt');
      $filesize = array('307200');
      break;

  
  
    case 'Brazil':
      $path = drupal_lookup_path('source','office/brazil');
      //Deal with Page Personel
      if(empty($pp)) {
        $comments = TRUE;
        $filesize = array('307200');
      }
      //Michael Page
      else {
        //$type = TRUE;
        $filesize = array('512000');
      } 
      $terms=TRUE;
      $fileextensions = array('doc pdf rtf');
      break;

    case 'Canada':
      $path = drupal_lookup_path('source','office/canada');
      $comments = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('307200');
      break;

    case 'Colombia':
      $path = drupal_lookup_path('source','office/colombia');
      $comments = TRUE;
      $terms=TRUE;
      $fileextensions = array('doc txt rtf');
      $filesize = array('307200');
      break;

    case 'France':
      $path = drupal_lookup_path('source','office/france');
      $comments = TRUE;
      $telephone = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('512000');
      break;

    case 'Italy':
      $path = drupal_lookup_path('source','office/italy');
      $comments = TRUE;
      $type = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Middle East':
      $path = drupal_lookup_path('source','office/middle-east');
      $terms=TRUE;
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'Netherlands':
      $path = drupal_lookup_path('source','office/netherlands');
      $dob = TRUE;
      $location = TRUE;
      $comments = TRUE;
      $visa = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Nordics':
      $path = drupal_lookup_path('source','office/nordics');
      $comments = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Norway':
      $path = drupal_lookup_path('source','office/norway');
      $comments = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('1048576');
      break;

    case 'Qatar':
      $path = drupal_lookup_path('source','office/qatar');
      $fileextensions = array('doc docx rtf txt');
      $terms = TRUE;
      $filesize = array('2097152');
      break;

    case 'Singapore':
      $path = drupal_lookup_path('source','office/singapore');
      $comments = TRUE;
      $terms=TRUE;
      $fileextensions = array('doc pdf rtf');
      $filesize = array('307200');
      break;

    case 'Spain':
      $path = drupal_lookup_path('source','office/spain');
      $addr = TRUE;
      $dob = TRUE;
      $comments = TRUE;
      $telephone = TRUE;
      $terms = TRUE;
      $fileextensions = array('doc docx pdf rtf txt html');
      $filesize = array('512000');
      break;

    case 'South Africa':
      $path = drupal_lookup_path('source','office/south-africa');
      $terms=TRUE;  
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'UAE':
      $path = drupal_lookup_path('source','office/uae');
      $terms=TRUE;
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
      break;

    case 'UK':
      $path = drupal_lookup_path('source','office/uk');
      $terms=TRUE;
      $fileextensions = array('doc docx rtf txt');
      $filesize = array('2097152');
     
      break;

    case 'USA':
      $path = drupal_lookup_path('source','office/usa');
      $terms=TRUE;
      $fileextensions = array('doc docx pdf rtf txt');
      $filesize = array('307200');
      break;

  }

  //Shown fields
  $form['afj_first_name'] = array(
    '#title' => t('First Name'),
    '#type' => 'textfield',
    '#default_value' => t('First Name'),
  );

  $form['afj_last_name'] = array(
    '#title' => t('Last Name'),
    '#type' => 'textfield',
    '#default_value' => t('Last Name'),
  );

  $form['afj_email_address'] = array(
    '#title' => t('Email Address'),
    '#type' => 'textfield',
    '#default_value' => t('Email'),
  );


  $nid_arr = explode("/",$path);
  $node = node_load((int)$nid_arr[1]);

  if(!empty($node->field_country_disclaimer)) {
    $disclaimer = TRUE;
    $disclaimerText = '<div class="to-be-filled">'.$node->field_country_disclaimer['und'][0]['safe_value'].'</div>';
  }
  if(!empty($node->field_country_tac)) {
    $termsText = '<div class="to-be-filled">'.$node->field_country_tac['und'][0]['safe_value'].'</div>';
  }
  $nid_arr = null;

  $filedescriptor = 'Identify the location of your CV (.'.str_replace(' ',' .',$fileextensions[0]).') size of '.formatBytes((double) $filesize[0],0);
  //Hidden file "stuff"
  $form['afj_extensions'] = array(
    '#type' => 'hidden',
    '#default_value' => $fileextensions,
  );

  $form['afj_size'] = array(
    '#type' => 'hidden',
    '#default_value' => $filesize,
  );

  //File (Compulsory)
  $form['afj_upload'] = array(
    '#name' => 'files[afj_upload_1]',
    '#type' => 'file',
    '#title' => t('Upload your CV'),
    '#description' => $filedescriptor,
    '#size' => 22,

  );

  if($telephone) {
    $form['afj_telephone'] = array(
      '#title' => t('Telephone'),
      '#type' => 'textfield',
      '#default_value' => t('Please enter your phone number'), 
    );  
  }

  if($comments) {
    $form['afj_comments'] = array(
      '#title' => t('Comments'),
      '#type' => 'textarea',
      '#resizable' => FALSE,
      '#description' => t(''),
      '#default_value' => t('Comments'),
    );  
  }
  
  if($current) {
    $form['afj_current'] = array(
      '#title' => t('Current job title'),
      '#type' => 'textfield',
      '#default_value' => t('Current job title'),
    );
  }

  if($location) {
    $form['afj_location'] = array(
      '#title' => t('Location'),
      '#type' => 'textfield',
      '#default_value' => t('Location'),
    );
  }

  if($disclaimer) {
    $form['ajf_disclaimer'] = array(
      '#title' => t('Disclaimer'),
      '#type' => 'markup',
      '#markup' => $disclaimerText, 
    );
  }
  

  if($visa) {
    $form['afj_euvisa'] = array(
      '#type' => 'checkbox',
      '#title' => t('I have a valid EU visa / working permit'),
      '#default_value' => FALSE,
    ); 
  }

  if($dob) {
    //Get today's dates (Server time will suffice as it's highly unlikely someone 
    //born "today" will apply for a job)
    $today = date('d-m-Y');

    $form['afj_date'] = array(
      '#type' => 'date_popup',
      '#title' => t('Date of birth'),
      '#description' => t('Date of Birth (dd-mm-yyyy) : '),
      '#date_format' => 'd-m-Y',
      '#date_year_range' => '1900:0',
      '#datepicker_options' => array(
        'minDate' => '01-01-1900',
        'maxDate' => $today,
      ),
    ); 
  }

  if($jlt) {
    $form['afj_jobslikethis'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send me more jobs like this'),
      '#default_value' => FALSE,
    );
  }

  if($mobile) {
    $form['afj_mobile'] = array(
      '#title' => t('Mobile'),
      '#type' => 'textfield',
      '#default_value' => t('Please enter your mobile phone number'), 
    );
  }

  if($addr) {
    $form['afj_address']['thoroughfare'] = array(
      '#type' => 'textfield',
      '#title' => t('Street'),
      '#default_value' => t('Street address'),
    );
    $form['afj_address']['locality'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' => t('City'),
    );
    $form['afj_address']['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Postcode'),
      '#default_value' => t('Postcode'),
    );
    $form['afj_address']['#tree'] = TRUE;
  }

  if($type) {
    $options = array('MJOB' => t('Please contact me for other relevant positions'), 'SJOB' => t('Please consider my application for this position only'));
    $form['afj_type'] = array(
      '#type' => 'radios',
      '#title' => t('In order to complete your application, please select one of the following options:'),
      '#default_value' => FALSE,
      '#options' => $options,
    );
  }

  

  if(!empty($termsText)) {
    $form['ajf_terms_text'] = array(
      '#title' => t('Terms'),
      '#type' => 'markup',
      '#markup' => $termsText, 
    );  
  }
  if($terms) {
    $form['afj_terms'] = array(
      '#type' => 'checkbox',
      '#title' => t('I agree with the Terms &amp; Conditions'),
      '#default_value' => FALSE,
    );
  }
  $form['submit'] = array(
     '#type' => 'submit',
     '#value' => 'Submit CV',
   );
  return $form;
}

function mp_submit_cv_job_apply_form_validate($form, &$form_state) {
  //Validation is handled by JS function here for posterity
  
  //validate the CV and "Save" it
  $values = $form_state['values'];
  $extensions = array($values['afj_extensions']);
  $size = array($values['afj_size']);
  
  $validators = array(
     'file_validate_size' => $size,
     'file_validate_extensions' => $extensions,
  );
  //save file as temporary
  $file = file_save_upload('afj_upload_1', $validators);
  
  if($file) {
    $file = file_move($file, 'public://'.strtr($file->filename,' ','_'));
    if($file) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t('Failed to write the uploaded file to the site\'s file folder.'));
    }
  }
  
  $form_state['storage']['file'] = $file;
}

function mp_submit_cv_job_apply_form_submit($form, &$form_state) {
  global $base_url;
  $backoffice = $form_state['values']['afj_backoffice'];


  $user = user_load($form_state['values']['afj_user']);
  $node = node_load($form_state['values']['afj_node']);
  $qsa = array(
    'job_ref' => $form_state['values']['afj_job_ref'],
  );

  //Work out where to send the mail
  $host = preg_replace('#^https?://#', '', strtolower($_SERVER['SERVER_NAME'])); //strtolower($base_url));

  if($host == 'oilandgas.page.com') {
    $mode = 'live';
  }
  //Staging - DT and SSM
  elseif ($host == 'mpoag.golleyslaterdigital.co.uk') {
    $mode = 'staging';
  }
  //DT Dev
  elseif ($host == 'mpoag.cfcs.co.uk'){
    $mode = 'dt-dev';
  }
  //SSM Dev
  elseif ($host == 'localhost') {
    $mode = 'ssm-dev';
  }
  
  //Choose template based off country
  $mailArray = _create_job_apply_email_from_country($form_state['values'], $backoffice ,$mode,$node,$user);

  switch($mode) {
    case 'live' :
      $to_address = $mailArray['to_address'].',lisakesson@michaelpage.com';
      break;

    case 'staging':
      $blacklist = array('dthorne@contemporaryfusion.co.uk','stevemason@golleyslater.co.uk','ssmason@gmail.com','mtemple@golleyslater.co.uk','david@cferthorney.com','david@davidthorne.net','mel_at_a_distance@hotmail.com');
      if(!in_array($form_state['values']['afj_email_address'], $blacklist)) {
        $to_address = $mailArray['to_address'].',lisakesson@michaelpage.com,venkatreddy@michaelpage.com,dthorne@contemporaryfusion.co.uk,SteveMason@golleyslater.co.uk,ssmason@gmail.com,mtemple@golleyslater.co.uk,mel_at_a_distance@hotmail.com';
      } 
      else {
        $to_address = 'dthorne@contemporaryfusion.co.uk,SteveMason@golleyslater.co.uk,ssmason@gmail.com,mel_at_a_distance@hotmail.com';  
      }
      break;

    case 'dt-dev':
      $to_address = 'dthorne@contemporaryfusion.co.uk,ssmason@gmail.com';  
      break;

    case 'ssm-dev':
      $to_address = 'stevemason@golleyslater.co.uk';
      break;

  }

  $headers = $mailArray['headers'];
  $apply_headers = $mailArray['apply_headers'];

  //save file as temporary
  $file = $form_state['storage']['file'];
  
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['file']);
  $location = DRUPAL_ROOT.'/'.variable_get('file_public_path').'/'.$file->filename;
   
  //Send email
  if($mailArray['nl2br'] == 'nl2br') {
    drupal_mail_wrapper_attachment('mail',$to_address,$mailArray['subject'],nl2br($mailArray['body']),$mailArray['from_address'],$mailArray['from_name'],$headers,$mailArray['charset'],array($location));
    if(!empty($mailArray['apply_to']) && !empty($mailArray['apply_subject']) && !empty($mailArray['apply_body'])) {
      drupal_mail_wrapper_attachment('mail',$mailArray['apply_to'],$mailArray['apply_subject'],nl2br($mailArray['apply_body']),'oilandgasfeedback@michaelpage.com.br','Michael Page Oil & Gas',$apply_headers,$mailArray['charset'],array());
    }
  }
  else {
    drupal_mail_wrapper_attachment('mail',$to_address,$mailArray['subject'],$mailArray['body'],$mailArray['from_address'],$mailArray['from_name'],$headers,$mailArray['charset'],array($location)); 
    if(!empty($mailArray['apply_to']) && !empty($mailArray['apply_subject']) && !empty($mailArray['apply_body'])) {
      drupal_mail_wrapper_attachment('mail',$mailArray['apply_to'],$mailArray['apply_subject'],$mailArray['apply_body'],'oilandgasfeedback@michaelpage.com.br','Michael Page Oil & Gas',$apply_headers,$mailArray['charset'],array());
    }
  }
    
  //Delete file
  file_delete($file);

  $form_state['redirect'] = array(
    'jobs/thank-you', 
    array(
      'query' => $qsa,
    ),
    302,
  );
}
