/**
 *  SmartSource shared
 */

var gImages=new Array;
var gIndex=0;
var DCS=new Object();
var DCSext=new Object();
var gDomain="trends.michaelpage.com";
var gDcsId="";
if (gDcsId==""){
	var gTagPath=gDomain;
}else{
	var gTagPath=gDomain+"/"+gDcsId;
}
function dcsVar(){
	var dCurrent=new Date();
	WT.tz=dCurrent.getTimezoneOffset()/60*-1;
	WT.bh=dCurrent.getHours();
	WT.ul=navigator.appName=="Netscape"?navigator.language:navigator.userLanguage;
	if (typeof(screen)=="object"){
		WT.cd=screen.colorDepth;
		WT.sr=screen.width+"x"+screen.height;
	}
	if (typeof(navigator.javaEnabled())=="boolean"){
		WT.jo=navigator.javaEnabled()?"Yes":"No";
	}
	WT.ti=document.title;
	WT.js="Yes";
	if (typeof(gVersion)!="undefined"){
		WT.jv=gVersion; // removed
	}

	DCS.dcsuri=window.location.pathname;
	DCS.dcsqry=window.location.search;

	WT.mpFlashVersion=''+getFlashVersion();

	if ((window.document.referrer!="")&&(window.document.referrer!="-")){
		if (!(navigator.appName=="Microsoft Internet Explorer"&&parseInt(navigator.appVersion)<4)){
			DCS.dcsref=window.document.referrer;
		}
	}
	DCS.dcssip=window.location.hostname;
	DCS.dcsdat=dCurrent.getTime();
}

function A(N,V){
	return "&"+N+"="+escape(V);
}
function dcsCreateImage(dcsSrc){
	if (false){
		document.writeln('SmartSource:\n'+ dcsSrc);
	} else if (document.images){
		gImages[gIndex]=new Image;
		gImages[gIndex].src=dcsSrc;
		gIndex++;
	}else{
		document.write('<img border="0" name="DCSIMG" width="1" height="1" src="'+dcsSrc+'" />');
	}
}
function dcsMeta(){
	var myDocumentElements;
	if (document.all){
		myDocumentElements=document.all.tags("meta");
	}else if (document.documentElement){
		myDocumentElements=document.getElementsByTagName("meta");
	}
	if (typeof(myDocumentElements)!="undefined"){
		for (var i=1;i<=myDocumentElements.length;i++){
			myMeta=myDocumentElements.item(i-1);
			if (myMeta.name){
				if (myMeta.name.indexOf('WT.')==0){
					WT[myMeta.name.substring(3)]=myMeta.content;
				}else if (myMeta.name.indexOf('DCSext.')==0){
					DCSext[myMeta.name.substring(7)]=myMeta.content;
				}else if (myMeta.name.indexOf('DCS.')==0){
					DCS[myMeta.name.substring(4)]=myMeta.content;
				}
			}
		}
	}
}
function dcsTag(TagImage){
	var P="http"+(window.location.protocol.indexOf('https:')==0?'s':'')+"://"+TagImage+"/dcs.gif?";
	for (N in DCS){
		if (DCS[N]) {
			P+=A(N,DCS[N]);
		}
	}
	for (N in WT){
		if (WT[N]) {
			P+=A("WT."+N,WT[N]);
		}
	}
	for (N in DCSext){
		if (DCSext[N]) {
			P+=A(N,DCSext[N]);
		}
	}
	if (P.length>2048&&navigator.userAgent.indexOf('MSIE')>=0){
		P=P.substring(0,2040)+"&WT.tu=1";
	}
	dcsCreateImage(P);
}

function getFlashVersion() {
  var hasFlash = 0;
  if (navigator.plugins && navigator.plugins.length) {
    plgFlash = navigator.plugins["Shockwave Flash"];
    if (plgFlash) {
      if (plgFlash.description) {
        plgDesc = plgFlash.description;
        hasFlash = plgDesc.charAt(plgDesc.indexOf('.')-1);
      }
    } else {
      hasFlash = -1;
    }
    if (navigator.plugins["Shockwave Flash 2.0"]) {
      hasFlash = 2;
    }
  } else if (navigator.mimeTypes && navigator.mimeTypes.length) {
    mimeFlash = navigator.mimeTypes['application/x-shockwave-flash'];
    if (mimeFlash && mimeFlash.enabledPlugin) {
      hasFlash = 2;
    } else {
      hasFlash = -1;
    }
  } else if ( navigator.userAgent.toLowerCase().indexOf("msie 2") != -1 || navigator.userAgent.toLowerCase().indexOf("msie 3") != -1 || navigator.userAgent.toLowerCase().indexOf("msie 4") != -1 ) {
    hasFlash = 0; // not detecting on IE < 5
  } else if ( navigator.userAgent.toLowerCase().indexOf("msie") != -1 && parseInt(navigator.appVersion) >= 4 && navigator.userAgent.toLowerCase().indexOf("win")!=-1 ) {
    for(var vLoop=9; vLoop>1; vLoop--){
      try {
        var objFlash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash." + vLoop);
        hasFlash = vLoop;
        break;
      } catch(e) { }
    }
  }
  return hasFlash;
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

