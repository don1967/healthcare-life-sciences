/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* changed node-type-jobs to context-jobs */

 
function SortByName(a, b){
  var aName = a.name.toLowerCase();
  var bName = b.name.toLowerCase(); 
  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
       
/* form validation messages */
var form_errors = new Array();
form_errors['forename'] = "Please enter your name<br/>";
form_errors['lastname'] = "Please enter your family name<br/>";
form_errors['email']    = "Please enter a valid email<br/>";
form_errors['file']     = "Please select a cv to upload<br/>";
form_errors['terms']    = "Please check you have read terms & conditions<br/>";
form_errors['region']   = "Please select the regions your interested in working in<br/>";


var cntryText = new Array();

cntryText['Africa Africa']='<p class="lintro">Michael Page Oil & Gas has a team dedicated to oil and gas recruitment in Africa. Based in Paris, we have a proven track record of attracting the best engineering and technical talent to a range of employers.<br/><br/><a href="/office/Africa" class="map-location">Africa</a></p>';
cntryText['Africa']='<p class="lintro">Michael Page Oil & Gas has a team dedicated to oil and gas recruitment in Africa. Based in Paris, we have a proven track record of attracting the best engineering and technical talent to a range of employers.<br/><br/><a href="/office/Africa" class="map-location">Africa</a></p>';

cntryText['Asia Pacific']='<p class="lintro">In Asia Pacific we recruit technical and engineering oil and gas jobs across the region from our offices in Australia and Singapore.<br/><br/><a href="/office/Australia" class="map-location">Australia</a><br/><a href="/office/Singapore" class="map-location">Singapore</a></p>';
cntryText['Europe']='<p class="lintro">Want to recruit technical and engineering oil and gas candidates? Please get in touch – we have offices across Europe.<br/><br/><a href="/office/France" class="map-location">France</a><br/><a href="/office/Italy" class="map-location">Italy</a><br/><a href="/office/Netherlands" class="map-location">Netherlands</a><br/><a href="/office/Nordics" class="map-location">Nordics</a><br/><a href="/office/Spain" class="map-location">Spain</a><br/><a href="/office/UK" class="map-location">UK</a></p>';
cntryText['Latin America']='<p class="lintro">Michael Page Oil & Gas has a large team of consultants dedicated to the technical oil and gas market in Latin America, based in offices across Brazil and Colombia.<br/><br/><a href="/office/Brazil" class="map-location">Brazil</a><br/><a href="/office/Colombia" class="map-location">Colombia</a></p>';
cntryText['Middle East']='<p class="lintro">Michael Page Oil & Gas recruits technical and engineering roles for a range of employers. We have offices in the UAE and Qatar, but our remit extends across the Middle Eastern region.<br/><br/><a href="/office/UAE" class="map-location">UAE</a><br/><a href="/office/Qatar" class="map-location">Qatar</a></p>';
cntryText['North America']='<p class="lintro">In North America we recruit technical and engineering oil and gas jobs across the region from our offices in Canada and the USA.<br/><br/><a href="/office/Canada" class="map-location">Canada</a><br/><a href="/office/USA" class="map-location">USA</a></p>';
 



jQuery(document).ready(function() {
  
  
  
         /* remove location and add to the title of job */
         if (jQuery(".view-job-search-page").length>0) {
             jQuery(".views-row").each(function() {
                  
                  var title = jQuery(this).find(".views-field-title .field-content a").text();
                  var location = jQuery(this).find(".views-field-field-job-location-1 .field-content").text();   
                  
                if (location.length>0) {
                  var newTitle = title + " - " + location;
                } else {
                    var newTitle = title;
                }
                  jQuery(this).find(".views-field-title .field-content a").text(newTitle)
          jQuery(this).find(".views-field-field-job-location-1").remove();
                  
             })
         }
  
  
  
  
         if (jQuery(".url-looking-job .region-content").length>0) {            
          
            jQuery(".url-looking-job .region-content").wrap('<div class="content-container">');
            jQuery(".url-looking-job .content-container").wrap('<div class="header-content-container">');
            jQuery(".url-looking-job .header-content-container").wrap('<div class="footer-container">');
            
         }
         if (jQuery(".url-search-job .region-content").length>0) {            
            jQuery(".url-search-job .region-content").wrap('<div class="content-container">');
            jQuery(".url-search-job .content-container").wrap('<div class="header-content-container">');
            jQuery(".url-search-job .header-content-container").wrap('<div class="footer-container">');
            
         }
  
         if (jQuery(".context-jobs").length>0) {
             
             var content= jQuery(".context-jobs .region-content").html()
            
             var backURL = jQuery('input[name="afj_search"]').val();
             if ( backURL) {
                  var backButton = '<a href="" class="back-oneX">back</a>';
                  jQuery(".context-jobs .region-content").html(backButton + content);
                  jQuery(".back-oneX").attr("href", "/looking-job"+backURL);
             } else {
                  var backButton = '<a href="" class="back-one">back</a>';
                  jQuery(".context-jobs .region-content").html(backButton + content);
                  jQuery(".back-one").attr("href", "/looking-job");  
             }
             var link = jQuery(".field-name-field-tax-regions .field-items .field-item a").text();
             jQuery(".field-name-field-tax-regions .field-items .field-item").html(link);
              
             var subsector = jQuery(".field-name-field-tax-subsector .field-items .field-item a").text();
             jQuery(".field-name-field-tax-subsector .field-items .field-item").html(subsector);
             
             
         }
//         
//         jQuery("a.back-one").live("click",function() {
//             parent.history.back(2);
//             return false;
//         })
         
  
          /* jquery transform calling */
          if(jQuery("form#views-exposed-form-job-search-page-job-search-page").length>0) {  
 

  
            // NOTE: the exposed filter was given a filter identifier of "slider_filter".  Views
            // adds the "edit-" and "-min/-max" parts.  
            var min = jQuery('input#edit-field-job-min-sal');
            var max = jQuery('input#edit-field-job-max-sal');

            if (!min.length || !max.length) {
              // No min/max elements on this page
              return;
            }

            // Set default values or use those passed into the form
            var init_min = ('' == min.val()) ? 0 : min.val();
            var init_max = ('' == max.val()) ? 500000 : max.val();

            // Set initial values of the slider 
            min.val(init_min);
            max.val(init_max);

            // Insert the slider before the min/max input elements 
            min.parents('div.views-widget-filter-field_job_usd_sal').before(
              jQuery('<div></div>').slider({
                range: true,
                min: 10000,     // Adjust slider min and max to the range 
                max: 500000,    // of the exposed filter.
                step:50000,
                values: [init_min, init_max],
                slide: function(event, ui){
                  // Update the form input elements with the new values when the slider moves
                  min.val(ui.values[0]);
                  max.val(ui.values[1]);
//                  var minxx = jQuery("#edit-field-job-max-sal").val();
//                  jQuery("#edit-field-job-max-sal").val(minxx);
                  }
              })
            );  // Add .hide() before the ';' to remove the input elements altogether.

           
            
            jQuery("form#views-exposed-form-job-search-page-job-search-page").jqTransform();
             
            jQuery("#edit-field-job-min-sal").parent().css( "background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/survey/slider_down.gif') right no-repeat");
            jQuery("#edit-field-job-max-sal").parent().css( "background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/survey/slider_up.gif') no-repeat");
          }
          
          
          if(jQuery("form#views-exposed-form-job-search-page-job-search-landing-page").length>0) {
            
            var check  = getUrlVars()["field_job_title"];
            if (check) {
            
                jQuery(".header-content-container").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/backgrounds/looking_without_header.jpg') no-repeat scroll 0 0 transparent")
                jQuery(".header-content-container").css("padding","76px 0 0");
                jQuery(".content-container").css("min-height","610px");
            }
            
            
            if (jQuery("#edit-field-job-region").length>0) {
             jQuery("#edit-field-job-region option").each(function() {
                 
                 if ((jQuery(this).text()=="-UK")  || (jQuery(this).text()=="Other")) {
                     jQuery(this).remove();
                 }
             }) 
            }
            
             jQuery("#edit-field-job-title, #edit-field-job-location").focus(function() {               
                    if ((jQuery(this).val()=='Job title') || (jQuery(this).val()=='Location') ) {
                        jQuery(this).val("");
                        jQuery(this).css("font-style","normal")
                        jQuery(this).css("color","#000") 
                    }
             })  
            
             jQuery("#edit-field-job-title").blur(function() {
                if (jQuery(this).val()=='') {
                    jQuery(this).val("Job title");
                    jQuery(this).css("font-style","italic")
                        jQuery(this).css("color","#666")
                   
                }  
                })
             jQuery("#edit-field-job-location").blur(function() {
                if (jQuery(this).val()=='') {
                    jQuery(this).val("Location");
                    jQuery(this).css("font-style","italic")
                        jQuery(this).css("color","#666")
                }  
            })
            if(jQuery("#edit-field-job-title").val() == '' ) {
              jQuery("#edit-field-job-title").attr("value", "Job title");
            }
            
            if(jQuery("#edit-field-job-location").val() == '' ) {
              jQuery("#edit-field-job-location").attr("value", "Location");
            }
              // NOTE: the exposed filter was given a filter identifier of "slider_filter".  Views
            // adds the "edit-" and "-min/-max" parts.  
            
            
             
            var min = jQuery('input#edit-field-job-min-sal');
            var max = jQuery('input#edit-field-job-max-sal');

            if (!min.length || !max.length) {
              // No min/max elements on this page
              return;
            }

            // Set default values or use those passed into the form
            var init_min = ('' == min.val()) ? 0 : min.val();
            var init_max = ('' == max.val()) ? 500000 : max.val();

            // Set initial values of the slider 
            min.val(init_min);
            max.val(init_max);

            // Insert the slider before the min/max input elements 
            min.parents('div.views-widget-filter-field_job_usd_sal').before(
              jQuery('<div></div>').slider({
                range: true,
                min: 0,     // Adjust slider min and max to the range 
                max: 500000,    // of the exposed filter.
                step:50000,
                values: [init_min, init_max],
                slide: function(event, ui){
                  // Update the form input elements with the new values when the slider moves
                  min.val(ui.values[0]);
                  max.val(ui.values[1]);
//                  var minxx = jQuery("#edit-field-job-max-sal").val();
//                  jQuery("#edit-field-job-max-sal").val("$"+minxx);
                  }
              })
            );  // Add .hide() before the ';' to remove the input elements altogether.

            if (jQuery(".ui-slider-horizontal").length>0) {
              var first=jQuery('input#edit-field-job-min-sal');
              
              jQuery("a.ui-slider-handle").each(function() {                   
                jQuery(this).html(first);
                 
                first = jQuery('input#edit-field-job-max-sal');
                
              })
             
            }
            jQuery("form#views-exposed-form-job-search-page-job-search-landing-page").jqTransform();
            jQuery("#edit-field-job-min-sal").parent().css( "background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/survey/slider_up.png') right no-repeat");
            jQuery("#edit-field-job-max-sal").parent().css( "background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/survey/slider_down.png') 0 5px no-repeat");
            var current = jQuery("#edit-field-job-title").val();
            // validate form to check title has been entered
            // jQuery("form#views-exposed-form-job-search-page-job-search-landing-page #edit-submit-job-search-page").click(function() {                  
            //     var title = jQuery("#edit-field-job-title");  
            //     if ((title.val()=='') || (title.val()==current)) {
            //       title.css("border","1px solid #fb7204");
            //       return false;
            //     }
            //     return true;
            // })
              
              
          }
          
          /* survey problem below */
          
          
          if (jQuery("form#mp-searchform-quicksearch-form").length>0) {             
              jQuery("form#mp-searchform-quicksearch-form").jqTransform();
              
              jQuery("#edit-field-job-title, #edit-field-job-location").live("focus", function() {
                   
                    if ((jQuery(this).val()=='Job Title') || (jQuery(this).val()=='Location') ) {
                        jQuery(this).val("");
                        jQuery(this).css("font-style","normal")
                        jQuery(this).css("color","#000")
                    }
                })  
//              
                jQuery("#edit-field-job-title").live("blur",function() {
                if (jQuery(this).val()=='') {
                    jQuery(this).val("Job Title");
                    jQuery(this).css("font-style","italic");
                    jQuery(this).css("color","#666");
                }  
                })
                jQuery("#edit-field-job-location").live("blur",function() {
                    if (jQuery(this).val()=='')  {
                        jQuery(this).val("Location");
                        jQuery(this).css("font-style","italic");
                        jQuery(this).css("color","#666");
                    }  
                    
                })
              
              
              
              
              
          }
          if (jQuery("form#mp-searchform-advancedsearch-form").length>0) { 


            if (jQuery("#edit-field-job-regions").length>0) {
             jQuery("#edit-field-job-regions option").each(function() {
                 if (jQuery(this).text()=="Other") {
                     jQuery(this).remove();
                 }
             }) 
            }



             // NOTE: the exposed filter was given a filter identifier of "slider_filter".  Views
             // adds the "edit-" and "-min/-max" parts.  
           
             var min = jQuery('input#edit-field-job-min-sal');
             var max = jQuery('input#edit-field-job-max-sal');

             if (!min.length || !max.length) {
                 // No min/max elements on this page
                 return;
             }

             // Set default values or use those passed into the form
             var init_min = ('' == min.val()) ? 0 : min.val();
             var init_max = ('' == max.val()) ? 500000 : max.val();

             // Set initial values of the slider 
             min.val(init_min);
             max.val(init_max);

             // Insert the slider before the min/max input elements 
             min.parents('div.form-item-field-job-min-sal').before(
                 jQuery('<div></div>').slider({
                 range: true,
                 min: 0,     // Adjust slider min and max to the range 
                 max: 500000,    // of the exposed filter.
                 step:50000,
                 values: [init_min, init_max],
                 slide: function(event, ui){
                     // Update the form input elements with the new values when the slider moves
                     min.val(ui.values[0]);
                     max.val(ui.values[1]);
                 }
                 })
             );  // Add .hide() before the ';' to remove the input elements altogether.
          
                jQuery("#edit-field-job-title, #edit-field-job-location").focus(function() {
                   
                    if ((jQuery(this).val()=='Job Title') || (jQuery(this).val()=='Location') ) {
                        jQuery(this).val("");
                        jQuery(this).css("font-style","normal");
                        jQuery(this).css("color","#000");
                         
                    }
                })  
//              
                jQuery("#edit-field-job-title").blur(function() {
                   if (jQuery(this).val()=='') {
                    jQuery(this).val("Job Title");
                    jQuery(this).css("font-style","italic");
                    jQuery(this).css("color","#666");
                     
                    }  
                })
                jQuery("#edit-field-job-location").blur(function() {
                    if (jQuery(this).val()=='')  {
                        jQuery(this).val("Location");
                        jQuery(this).css("font-style","italic");
                        jQuery(this).css("color","#666");
                    }  
                    
                })
              
                if (jQuery(".ui-slider-horizontal").length>0) {


                   var first=jQuery('input#edit-field-job-min-sal');
                   jQuery("a.ui-slider-handle").each(function() {                   
                       jQuery(this).html(first);
                       first = jQuery('input#edit-field-job-max-sal');
                   })



                }
              
              
              /* end some where here */
              
              
              jQuery("form#mp-searchform-advancedsearch-form").jqTransform();
              var current = jQuery("#edit-field-job-title").val();
              
              // validate form to check title has been entered
              jQuery("form#mp-searchform-advancedsearch-form #edit-submit").click(function() {                  
                  var title = jQuery("#edit-field-job-title");  
                  if ((title.val()=='') || (title.val()==current)) {
                      
                      title.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req_miss.png')");
                    
                      return false;
                  } else {
                      title.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req.png')");
                  }
                  return true;
              })
              
              jQuery("#edit-field-job-min-sal").parent().css( "background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/survey/slider_right.png') right no-repeat");
              jQuery("#edit-field-job-max-sal").parent().css( "background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/survey/slider_left.png') no-repeat");
              
              
              
              
          }
 
          if (jQuery("form#mp-submit-cv-jsu-form").length>0) { 
              
              
             
                var title=jQuery(".url-job-hire-job-upload-container .block-inner  h2.block-title").text();
               
              var man = '<span class="mandatory-form">Mandatory fields are highlighted in green</span>';
              jQuery(".url-job-hire-job-upload-container h2.block-title").html(title+man);
              
              jQuery("#mp-submit-cv-job-apply-form > div").before("<div id='error-box'></div>");
              
              
                if (jQuery(".form-item-jsu-regions").length>0) {
                    jQuery(".form-item-jsu-regions select option").each(function() {
                        var cnt =1;
                        if (jQuery(this).text()=="Region") {
                            jQuery(this).text("Location");
//                            jQuery(".form-item-jsu-regions").scrollTop();
//                            jQuery(this).attr('selected', 'selected')
//                            jQuery(this).attr("selected", "selected")
                        }
                         if ((jQuery(this).text()=="USA")) {
                            jQuery(this).removeAttr("selected");
                          
                         }
                          if ((jQuery(this).val()=="")) {
                            jQuery(this).remove();
                          
                         }
                    })
                }
              
              
                jQuery(".url-job-hire-job-upload .block-main").wrap('<div class="url-job-hire-job-upload-container">');
                jQuery(".url-job-hire-job-upload-container #edit-submit").after('<div class="mp-submit-cv-csu-form-footer"></div>');
                
                var all = jQuery(".url-job-hire-job-upload-container").html();
                var intro = '<p class="intro" style="width:560px;font-size:11px;">Help us direct your enquiry by directing you to the most relevant Michael Page Oil & Gas contact.<br/><strong>Where would you like to source candidates?</strong><br/><span class="mandatory">Mandatory fields are highlighted in green</span></p><div id="error-box"></div>';
                jQuery(".url-job-hire-job-upload-container").html(intro + all);
                
//                jQuery("#mp-submit-cv-jsu-form > div").css("display","none");
                jQuery('form#mp-submit-cv-jsu-form').append('<div class="wb-container"><div id="gc"><input type="radio" name="source" class="source" value="global"/><span class="global-text">1. Globally</span><br /><br /></div><div id="sc"><input type="radio" name="source" value="specific" class="source"/><span class="global-text">2. In a specific location</span><br /><br /></div></div>');
              
                jQuery(":radio").live("click", function() { 
                    var selected = jQuery(this).val();
                    var ht = jQuery(".form-item-jsu-regions").html();                    
                    var message = '';
                    jQuery('<div class="msg">').insertBefore(jQuery(".form-item-jsu-regions"))
      
                    jQuery("div.msg").html("");
                    if (selected=="global") {
                      
                           jQuery("div.msg").html("Where are you based? Choose your closest Michael Page Oil & Gas office.");
                       
                       
                    } else {
                     
                          
                           jQuery("div.msg").html("Where is your role based? Choose your closest  Michael Page Oil & Page office");
                     
                      jQuery("form#mp-submit-cv-jsu-form").jqTransform();
                       
                    }
    
                })
                jQuery("form#mp-submit-cv-jsu-form").jqTransform();
             
              
                jQuery("input[type=file]").filestyle({ 
                            image: "https://oilandgas.page.com/sites/all/themes/mp/css/images/buttons/browse.jpg",
                            imageheight : 30,
                            imagewidth : 78,
                            width : 180,
                            height:78
                    });  
//                
               jQuery("#edit-jsu-first-name, #edit-jsu-last-name, #edit-jsu-email-address").focus(function() {
               
                    if ((jQuery(this).val()=='First Name') || (jQuery(this).val()=='Last Name') || (jQuery(this).val()=='Email') ) {
                         jQuery(this).val("");
                         jQuery(this).css("font-style","normal")
                             jQuery(this).css("color","#000")
                     }
                })  
                
                jQuery("#edit-jsu-first-name").blur(function() {
                    if (jQuery(this).val()=='') {
                        jQuery(this).val("First Name");
                        jQuery(this).css("font-style","italic");
                        jQuery(this).css("color","#666")
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");

                        } else {
                            jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        } 
                })
                jQuery("#edit-jsu-last-name").blur(function() {
                    if (jQuery(this).val()=='')  {
                        jQuery(this).val("Last Name");
                        jQuery(this).css("font-style","italic");
                        jQuery(this).css("color","#666")
                       jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                       
                    } else {
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                    } 
                })
                
                jQuery("#edit-jsu-email-address").blur(function() {
                        if (jQuery(this).val()=='') {
                            jQuery(this).val("Email");
                            jQuery(this).css("font-style","italic")
                            jQuery(this).css("color","#666")
                            jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                         } else {
                            if( !isValidEmailAddress( jQuery(this).val() ) ) {                            
                                jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                            } else {
                                jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                            }
                         } 
                })
                
                 
                
                
                jQuery("#edit-submit").click(function() {
                    
                    var errors=0;
                    error_string=""; 
                    var firstn = jQuery("#edit-jsu-first-name");
                    var fn     = jQuery("#edit-jsu-last-name");
                    var em     = jQuery("#edit-jsu-email-address");
                    
//                    var terms  = jQuery('#edit-afj-terms');

                    var fileU = jQuery("input:file");

                    
                    
//                    if (jQuery('#edit-afj-terms').is(':checked')) {
//                        jQuery(".form-item-afj-terms a").css("border","none");
//                    } else {
//                         jQuery(".form-item-afj-terms a").css("border","1px solid #fb7204");
//                         errors=1; 
//                    }

                    var hasSelectedRegion=0;
                    jQuery(".regions").each(function() {
                         if (jQuery(this).is(':checked')) { 
                             hasSelectedRegion=1;                             
                         } 
                    })

                    if (hasSelectedRegion==0) {
                        jQuery(".interests").css("border-bottom","1px solid #fb7204" )
                        
                    } else {
                       jQuery(".interests").css("border-bottom","1px solid #CCCCCC");
                    }
                    if ((firstn.val()=='') || (firstn.val()=='First Name')) {
                        error_string=error_string+form_errors['forename'];
                        firstn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                               
                        errors=1; 
                    } else {
                        firstn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        
                    }
                    if ((fn.val()=='') || (fn.val()=='Last Name')) {
                         fn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                       
                        error_string=error_string+form_errors['lastname'];
                        errors=1; 
                    } else {
                        fn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        
                    } 
                    if ((em.val()=='') || (em.val()=='Email')) {
                        em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                       
                        error_string=error_string+form_errors['email'];
                        errors=1; 
                    } else {
                       
                       
                       
                        if( !isValidEmailAddress( em.val() ) ) {
                            error_string=error_string+form_errors['email'];
                             errors=1;
                           em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                        
                        } else {
                            em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        
                        }
                       
                       
                    }
//                    if ((t.val()=='') || (t.val()=='Please enter your phone number')) {
//                        t.css("border-color","#fb7204");
//                        errors=1; 
//                    }  else {
//                        t.css("border-color","#c1c1c1");
//                    }

                    if (fileU.val()=='') {
                        error_string=error_string+form_errors['file'];
                       
                        jQuery("input.form-file").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat no-repeat");
                        
                        errors=1;  
                    } else {
                         jQuery("input.form-file").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        
                        
                    }
                    if (errors==1) {
                        jQuery("#error-box").html(error_string);
                        return false;
                    }
                    return true;
                    
                 
                    
                    
                    
                   
                })
          }
          if (jQuery("form#mp-submit-cv-job-apply-form").length>0) { 
              
              
              var title=jQuery(".block-mp-submit-cv-job-apply .block-inner  h2.block-title").text();
               
              var man = '<span class="mandatory-form">Mandatory fields are highlighted in green</span>';
              jQuery("#block-mp-submit-cv-mp-submit-cv-job-apply h2.block-title").html(title+man);
              
              jQuery("#mp-submit-cv-job-apply-form > div").before("<div id='error-box'></div>");
          
//                if (jQuery(".form-item-afj-terms label").length>0) {
//                    
//                    var link = jQuery(".form-item-afj-terms label").text();
//                   
//                    var check =  jQuery('<div class="switch">').insertAfter(jQuery(".form-item-afj-terms span"));
//                    
//                     
//                    jQuery(".switch").html(link);
//                }
            
                var footer = jQuery('<div class="mandatory">').insertAfter(jQuery("form#mp-submit-cv-job-apply-form")) ;
                //jQuery('.mandatory').html("*mandatory fields");
                jQuery("input[type=file]").filestyle({ 
                                image: "https://oilandgas.page.com/sites/all/themes/mp/css/images/buttons/browse.jpg",
                                imageheight : 30,
                                imagewidth : 70,
                                width : 180, 
                                height:78
                                
                    });  
                jQuery("form#mp-submit-cv-job-apply-form").jqTransform();

                jQuery("#edit-afj-first-name, #edit-afj-last-name, #edit-afj-email-address, #edit-afj-telephone, #edit-afj-location, #edit-afj-current").focus(function() {
               
               if ((jQuery(this).val()=='First Name') || (jQuery(this).val()=='Last Name') || (jQuery(this).val()=='Email') || (jQuery(this).val()=='Please enter your phone number')|| (jQuery(this).val()=='Location') || (jQuery(this).val()=='Current job title')) {
                    jQuery(this).val("");
                    jQuery(this).css("font-style","normal")
                    jQuery(this).css("color","#000")
                }
                })  
                jQuery("#edit-afj-first-name").blur(function() {
                if (jQuery(this).val()=='') {
                    jQuery(this).val("First Name");
                    jQuery(this).css("font-style","italic")
                    jQuery(this).css("color","#666");
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                       
                    } else {
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_job.png')");
                       
                    } 
                })
                if (jQuery("#edit-afj-last-name").length>0) {
                    jQuery("#edit-afj-last-name").blur(function() {
                        if (jQuery(this).val()=='')  {
                            jQuery(this).val("Last Name");
                            jQuery(this).css("font-style","italic")
                        jQuery(this).css("color","#666");
                            jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");

                        } else {
                            jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_job.png')");
                        } 
                    })
                }
                if (jQuery("#edit-afj-email-address").length>0) {
                jQuery("#edit-afj-email-address").blur(function() {
                    if (jQuery(this).val()=='') {
                        jQuery(this).val("Email");
                        jQuery(this).css("font-style","italic")
                        jQuery(this).css("color","#666");
                          jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                        } else { 
                            if( !isValidEmailAddress( jQuery(this).val() ) ) {                            
                                jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                                
                            } else {

                                    jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_job.png')");

                            }
                        
                    } 
                })
                }
                if (jQuery("#edit-afj-telephone").length>0) {
                    jQuery("#edit-afj-telephone").blur(function() {
                        if (jQuery(this).val()=='') {
                            jQuery(this).val("Please enter your phone number");
                            jQuery(this).css("font-style","italic")
                            jQuery(this).css("color","#666")
                        } else {
                            jQuery(this).css("border-color","#c1c1c1");
                        } 
                    }) 
                }
                
               jQuery("#edit-submit").live("click", function() {
                   
                    jQuery("#error-box").html("");
                    
                    var error_string="";
                    var errors=0;

                    var firstn = jQuery("#edit-afj-first-name");
                    var fn     = jQuery("#edit-afj-last-name");
                    var em     = jQuery("#edit-afj-email-address");
                    var t      = jQuery("#edit-afj-telephone");
                    var terms  = jQuery('#edit-afj-terms');

                    var fileU = jQuery("input:file");

                    
                    
                    if ((firstn.val()=='') || (firstn.val()=='First Name')) {
                        
                        firstn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                                
                       error_string=error_string+form_errors['forename'];
                        errors=1; 
                    } else {
                        
                        firstn.css("border-color","#c1c1c1");
                    }
                    if ((fn.val()=='') || (fn.val()=='Last Name')) {
                        error_string=error_string+form_errors['lastname'];
                        fn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                        errors=1; 
                    } else {
                        fn.css("border-color","#c1c1c1");
                    } 
                    
                    
                    
                    if ((em.val()=='') || (em.val()=='Email')) {
                         em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                         error_string=error_string+form_errors['email'];
                        errors=1; 
                    } else {
                        
                        if( !isValidEmailAddress( em.val() ) ) {
                            
                            error_string=error_string+form_errors['email'];
                            em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                            errors=1
                        } else {
                           em.css("border-color","#c1c1c1"); 
                        }
                        
                        
                        
                    }
                    
                    if (fileU.val()=='') {                         
                       
                         jQuery("input.form-file").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/free_text_req_miss.png')");
                        error_string=error_string+form_errors['file'];                        
                        errors=1;  
                    }
                    if (jQuery('#edit-afj-terms').length>0) {
                        if (jQuery('#edit-afj-terms').is(':checked')) {
//                            jQuery(".form-item-afj-terms").css("border","none");
                        } else {
                             error_string=error_string+form_errors['terms'];
//                             jQuery(".form-item-afj-terms").css("border","1px solid #fb7204");
                             errors=1; 
                        }
                    }
                     
//                        if ((t.val()=='') || (t.val()=='Please enter your phone number')) {
//                            t.css("border-color","#fb7204");
//                            errors=1; 
//                        } else {
//                            t.css("border-color","#c1c1c1");
//                        }
                    
                    if (errors==1) {
                        jQuery("#error-box").html(error_string);
                        return false;
                    }
                    return true;
                    
                }) 
          }
       
           if (jQuery("form#mp-submit-cv-csu-form").length>0) {  
              
            
                
                jQuery(".url-find-job-cv-upload .block-main").wrap('<div class="mp-submit-cv-csu-form-container">');
//                jQuery("form#mp-submit-cv-csu-form").after('<div class="mp-submit-cv-csu-form-footer">footer</div>');
//                
                
                
               var regionHTML='';
                var checkHTML;
                
                if (jQuery("#edit-csu-regions".length>0)) {
                    regionHTML = '<div class="regions-container"><table border="1" cellspacing="0" cellpadding="0" width="200"><tr><td valign="top" width="200">';    
                    jQuery("#edit-csu-regions option").each(function() {
                        if ((jQuery(this).text()!="Other") && (jQuery(this).text()!="Region")) {
                        regionHTML = regionHTML + '<div style="height:24px;width:200px"><input type="checkbox" name="regions[]" value="'+jQuery(this).text()+'" class="regions"/><span class="region-text">'+jQuery(this).text()+'</span></div>';
                        } 
                         
                    })
                    regionHTML = regionHTML + '</tr></table></div>';
                }
                
                
                
                if (jQuery("#edit-csu-subsectors".length>0)) {
                    
                    
                    var cnt = 1;
                    
                    /* create checkboxes from the subsectors */
                    checkHTML = '<div class="checkbox-container"><div class="th-container"><span class="areas">Your areas of expertise</span><span class="interests">Regions you\'re interested in working</span></div><table border="0" cellspacing="0" cellpadding="0" width="400"><tr><td valign="top" width="200">';                 
                    jQuery("#edit-csu-subsectors option").each(function() {   
                        if (jQuery(this).text()!="Subsector") {
                        checkHTML = checkHTML + '<div style="height:24px;width:230px"><input type="checkbox" name="subsectors[]" value="'+jQuery(this).text()+'" class="subsectors"/><span class="subsector-text">'+jQuery(this).text()+'</span></div>';
                        if (cnt==8) checkHTML = checkHTML + '</td><td valign="top">';
                        cnt=cnt+1;
                        }
                    }) 
                    checkHTML = checkHTML + '</tr></table></div>';
                    
                    /* add form html */
                    jQuery("form#mp-submit-cv-csu-form").append(checkHTML);
                    jQuery("form#mp-submit-cv-csu-form").append(regionHTML);
                    
                    
                    /* remove all the unwanted form fields and containers */
                    jQuery("#edit-csu-subsectors").remove();                    
                    jQuery("#addressfield-wrapper").remove() 
                    jQuery(".form-item-csu-full-name").remove() ;
                    jQuery(".form-item-csu-date").remove();    
                    jQuery(".form-item-csu-current").remove(); 
                }
                 
                
                var all = jQuery(".mp-submit-cv-csu-form-container").html();
                var intro = '<p class="intro" style="position:relative">Please fill in the form below, specify the locations that are of interest to you and attach a CV. Our recruitment teams will be in touch to discuss your career and job relevent opportunities<span class="mandatory-form-cv">Mandatory fields are highlighted in green</span></p>';
                jQuery(".mp-submit-cv-csu-form-container").html(intro + all);
                jQuery(".intro").after("<div id='error-box'></div>");
                jQuery("input[type=file]").filestyle({ 
                            image: "https://oilandgas.page.com/sites/all/themes/mp/css/images/buttons/browse.jpg",
                            imageheight : 30,
                            imagewidth : 70,
                            width : 180,
                            height:78
                });  
                jQuery("form#mp-submit-cv-csu-form").jqTransform();  
                
                
                
                jQuery("#edit-csu-first-name, #edit-csu-last-name, #edit-csu-email-address").focus(function() {
               
               if ((jQuery(this).val()=='First Name') || (jQuery(this).val()=='Last Name') || (jQuery(this).val()=='Email') ) {
                    jQuery(this).val("");
                    jQuery(this).css("font-style","normal");
                    jQuery(this).css("color","#000");
                    
                }
                })  
                
                 jQuery("#edit-csu-first-name").blur(function() {
                if (jQuery(this).val()=='') {
                    jQuery(this).val("First Name");
                    jQuery(this).css("font-style","italic");
                    jQuery(this).css("color","#666");
                    jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                       
                    } else {
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        jQuery(this).css("border-color","#c1c1c1");
                        jQuery(this).css("font-style","normal");
                     } 
                     
                    
                    
                })
                jQuery("#edit-csu-last-name").blur(function() {
                    if (jQuery(this).val()=='')  {
                        jQuery(this).val("Last Name");
                        jQuery(this).css("font-style","italic");
                        jQuery(this).css("color","#666");
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                       
                    } else {
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                        jQuery(this).css("border-color","#c1c1c1");
                        jQuery(this).css("font-style","normal");
                     } 
                     
                })
                jQuery("#edit-csu-email-address").blur(function() {
                    if (jQuery(this).val()=='') {
                        jQuery(this).val("Email");
                        jQuery(this).css("font-style","italic");
                        jQuery(this).css("color","#666");
                        jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                       
                    } else {
                        
                         
                        if( !isValidEmailAddress(  jQuery(this).val() ) ) {                             
//                            var error_string=error_string+form_errors['email'];
                            jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                            jQuery(this).css("font-style","normal");
                        } else {
                             
                             jQuery(this).css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                         
                        }
                        
                       
                     } 
                     
                })
                
                
                
                jQuery("#edit-submit").click(function() {
                    jQuery("#error-box").html(" ");
                    var errors=0;
                    var error_string="";
                    var firstn = jQuery("#edit-csu-first-name");
                    var fn     = jQuery("#edit-csu-last-name");
                    var em     = jQuery("#edit-csu-email-address");
                    
//                    var terms  = jQuery('#edit-afj-terms');

                    var fileU = jQuery("input:file");

                    
                    
                    
//                    if (jQuery('#edit-afj-terms').is(':checked')) {
//                        jQuery(".form-item-afj-terms a").css("border","none");
//                    } else {
//                         jQuery(".form-item-afj-terms a").css("border","1px solid #fb7204");
//                         errors=1; 
//                    }

                    var hasSelectedRegion=0;
                    jQuery(".regions").each(function() {
                         if (jQuery(this).is(':checked')) { 
                             hasSelectedRegion=1;                             
                         } 
                    })

                    
                    if ((firstn.val()=='') || (firstn.val()=='First Name')) {
                         firstn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                         error_string=error_string+form_errors['forename'];
                         errors=1; 
                    } else {
                         firstn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                          
                    }
                    if ((fn.val()=='') || (fn.val()=='Last Name')) {
                        fn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                        
                        error_string=error_string+form_errors['lastname'];
                        errors=1; 
                    } else {
                         fn.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                         
                    } 
                    if ((em.val()=='') || (em.val()=='Email')) {
                         em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                        
                        error_string=error_string+form_errors['email'];
                        errors=1; 
                    } else {
                         
                        if( !isValidEmailAddress( em.val() ) ) {
                            em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                        
                            error_string=error_string+form_errors['email'];
                            errors=1;
                        } else {
                           em.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                         
                        }
                        
                    }
                    if (hasSelectedRegion==0) {
                        jQuery(".interests").css("border-bottom","1px solid #fb7204" )
                        error_string=error_string+form_errors['region'];
                        errors=1;
                    } else {
                       jQuery(".interests").css("border-bottom","1px solid #CCCCCC");
                    }
//                    if ((t.val()=='') || (t.val()=='Please enter your phone number')) {
//                        t.css("border-color","#fb7204");
//                         errors=1; 
//                    }  else {
//                        t.css("border-color","#c1c1c1");
//                    }
                    if (fileU.val()=='') {
                         jQuery("input.form-file").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174_miss.png') no-repeat");
                         
                        
                        error_string=error_string+form_errors['file'];
                        errors=1;  
                    } else {
                          
                         jQuery("input.form-file").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/search/advanced_search_free_text_req174.png') no-repeat" );
                         
                    }
                    if (errors==1) {
                         jQuery("#error-box").html(error_string);
                        return false;
                    }
                    return true;
                    
                 
                    
                    
                    
                   
                })
                
                
                
                
               
          }
           
          
         /* secondary navigation styling of selected links */  
         if((jQuery(".url-about-us-case-studies").length>0) || (jQuery(".context-case-studies").length>0) || (jQuery(".page-about-us-case-studies").length>0)) { 
              jQuery("li.menu-624 a").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/navigation/Nav_About_ON.jpg')") ;
              if (jQuery(".field-type-image").length>0) {                  
                  var title = jQuery(".field-type-image img").attr("title");                   
              }
        }    
        if((jQuery(".context-jobs").length>0) || (jQuery(".url-jobs-thank-you").length>0)) { 
              jQuery("li.menu-626 a").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/navigation/Nav_Looking_ON.jpg')") ;
              
        } 
        if((jQuery(".page-market-updates-all").length>0) || (jQuery(".node-type-events").length>0) || (jQuery(".node-type-news").length>0) || (jQuery(".node-type-insights").length>0)) { 
              jQuery("li.menu-253 a").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/navigation/Nav_Market_ON.jpg')") ;
              // add footer to column to enable dynamic extention of page  
                if (jQuery(".field-name-field-tax-regions").length>0) {
                    var title = jQuery(".field-name-field-tax-regions .field-items .field-item a").text();
                    jQuery(".field-name-field-tax-regions .field-items .field-item").html(title);
                }
                
                if(jQuery(".marketupdates-all").length>0) {
                 var footer = jQuery('<div class="marketupdates-footer">').insertAfter(jQuery(".view-display-id-page_nei_all"))
              }
            
            
        }         
        if((jQuery(".node-type-office-country").length>0)) { 
              jQuery("li.menu-628 a").css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/navigation/Nav_Getintouch_ON.jpg')") ;
        }
 
 
 
 
 
        if (jQuery("form#views-exposed-form-views-nei-page-nei-all").length>0) {  
              // change the label               
              
             
                
                
              jQuery("label[for='edit-type-1']").text("Filter: News, Events, Insight");
              jQuery("form#views-exposed-form-views-nei-page-nei-all").jqTransform();              
              // add onChange so form submitted and button can be removed.
              jQuery("#edit-type-1, .form-type-select, .jqTransformSelectWrapper span").change(function() {
                   
                    jQuery(this).form.submit();
              });
        }
         
        /* swap title and intro in the survey */         
        if (jQuery(".view-display-id-block_survey_curr").length>0) { 
           
              /* swap */
              var txt = jQuery(".block-survey-views-block-survey-curr .field-name-field-advpoll-intro-text"); 
              txt.insertBefore(jQuery(".block-survey-views-block-survey-curr .field-name-title"))
              /* add the footer to the previous survey box */
              var container = jQuery(".view-display-id-block_survey_curr").html();
              
//                 jQuery('<div class="survey-footer"></div>').insertAfter('.view-display-id-block_survey_curr');
//              jQuery(".view-display-id-block_survey_curr").html(container+'<div class="survey-footer"></div>');
//              
              var title = jQuery(".block-survey-views-block-survey-curr .field-name-title .field-item h2").text();
              jQuery(".block-survey-views-block-survey-curr .field-name-title .field-item").html(title);
              
              
              
         }
         /* add the footer to the previous survey box */
         if (jQuery(".view-display-id-block_survey_curr").length>0) {               
              var container = jQuery(".block-survey-views-block-survey-prev");
//              var  footer = jQuery('<div class="survey-footer">').insertAfter(container);
         }
 
  
         /* quick search tab styling and functionality */
         if (jQuery(".block-mp-searchform-mp-searchform-quicksearch").length>0) {     
             
              // add in the quick search tab
              var slick = jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").parent().parent();
              var slickHTML =slick.html(); 
              var tabHTML   = '<div id="quick-search-tab">quick search</div>';
              jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").parent().parent().html(tabHTML+slickHTML);              
              // add a click box to quick search to enable close of quick search              
              var closeHTML = '<div id="edit-close">close search</div>';              
              var qsHTML = jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").html();
              jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").html(closeHTML+qsHTML);
              
              
             
              // add in the actions
              jQuery("#quick-search-tab").live("click", function() {
                  
                  
                  
                  jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").slideDown(); 
                   jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").css('zIndex', "2349");
                  jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").animate({"top" : "-=25px"});
                  jQuery("#quick-search-tab").slideUp();  
              })
               jQuery("#edit-close").live("click", function() {
                  jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").animate({"top" : "+=25px"});
                  jQuery(".block-mp-searchform-mp-searchform-quicksearch .block-inner").slideUp();                   
                  jQuery("#quick-search-tab").slideDown();  
              })
     
         }
   
  
  
          function setUpdatesHeader(validate) {              
              if (!validate)  { 
                    jQuery('.page-market-updates-all .marketupdates-all .view-content .views-row .views-field-type-1 .field-content').each(function() {
                        var type = jQuery(this).text();
                        if (type=="Events") {
                            jQuery(this).parent().parent().css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_event.jpg') no-repeat");
                        } else if (type=="Insights") {
                            jQuery(this).parent().parent().css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_insight.jpg') no-repeat");
                        } else if (type=="News") {
                            jQuery(this).parent().parent().css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_news.jpg') no-repeat");
                        }
                    })
              } else {
                    jQuery('.views-field-title .field-content a').css("color","#fff")
                        jQuery('.selected').each(function() {
                        var type = jQuery(this).find('.views-field-type-1 .field-content').text();                      
                        if (type=="Events") {
                            jQuery(this).css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_event.jpg') no-repeat");
                        } else if (type=="Insights") {
                            jQuery(this).css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_insight.jpg') no-repeat");
                        } else if (type=="News") {
                            jQuery(this).css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_news.jpg') no-repeat");
                        } 
                    })
              }
          }
        
          if (jQuery('.page-market-updates-all .marketupdates-all .view-content .views-row .views-field-type-1 .field-content').length>0) {
                setUpdatesHeader(0);
          }             
                         
          jQuery('.page-market-updates-all  .views-field-title .field-content a').live("click",function() {              
               
              var container = jQuery(this).parent().parent().parent();
              if (container.hasClass("selected")) {
              } else {
                 setUpdatesHeader(1);  
                 jQuery(this).css("color","#000");
                 var type = container.find('.views-field-type-1 .field-content').text();                  
                 if (type=="Events") {
                        jQuery(this).parent().parent().parent().css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_event_selected.jpg') no-repeat");
                    } else if (type=="Insights") {
                        jQuery(this).parent().parent().parent().css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_insight_selected.jpg') no-repeat");
                    } else if (type=="News") {
                        jQuery(this).parent().parent().parent().css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/market_updates/MARKET_concertina_news_selected.jpg') no-repeat");
                    }
                jQuery('.views-row').removeClass("selected"); 
                jQuery('.views-row').css("height","29px");                
                container.addClass("selected");              
                container.find('.views-field-field-exerpt, .views-field-nothing').slideDown(2100);
                container.css("height","auto");
               
              }
            
            return false;
          })
       
  
  
        /* News Events Insights carousel */
        
        
        if (jQuery('.view-display-id-page_nei_front').length>0) {
            
            jQuery('.url-market-updates  #controls a#previous').live("click",function() {return false;})            
            jQuery('.url-market-updates  #controls a#next').live("click",function() {return false;})
            
            /* add in the controls for carousel */
            
            var all = jQuery('.view-display-id-page_nei_front').parent().html();            
            var controls = '<div id="controls"><a href="" id="previous">Previous</a><a href="" id="next">Next</a><a id="viewall" href="/market-updates/all">View all news</a></div>';
            jQuery('.view-display-id-page_nei_front').parent().html(controls + all);
             
            /* add the container image */
            
            var wrapper = jQuery('.view-display-id-page_nei_front').parent();
            
            wrapper.parent().css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/case_studies/MARKET_landing_bg.jpg')") ;                   
            wrapper.parent().css("width","300px")  
             wrapper.parent().css("position","relative")  
            wrapper.parent().css("height","314px")                    
            wrapper.parent().css("padding-left","310px")                    
            wrapper.parent().css("margin-top","20px") 
            /* configure the carousel */
 
            jQuery('.view-display-id-page_nei_front').iosSlider({
                                            desktopClickDrag: true,
                                            snapToChildren: true, 
                                            autoSlide: false,
                                            scrollbarMargin: '0',
                                            scrollbarBorderRadius: '0',
                                            navPrevSelector: jQuery('.url-market-updates  #controls a#previous'),
					    navNextSelector: jQuery('.url-market-updates  #controls a#next') 
            }); 

              jQuery('.block-survey-views-block-survey-all').wrap('<div class="survey-block-container">');  
              jQuery('.block-survey-views-block-survey-all').iosSlider({
                                            desktopClickDrag: true,
                                            snapToChildren: true, 
                                            autoSlide: false,
                                            scrollbarMargin: '0',
                                            scrollbarBorderRadius: '0' 
            }); 
        }
        
        /* END News Events Insights carousel */
         
        if (jQuery('.view-display-id-page_casestudies').length>0) {            
           
            jQuery(".views-label-field-intro-brief").html("Brief");
            jQuery(".views-label-field-intro-we-delivered").html("We Delivered");
            jQuery(".views-field-field-case-teaser-intro,.views-field-field-case-outcome").css("display","none");            
            var wrapper = jQuery('.view-display-id-page_casestudies').parent();
            wrapper.parent().css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/case_studies/case_studies_carousel_back.jpg')") ;                   
            wrapper.parent().css("width","243px")                    
            wrapper.parent().css("height","359px")                    
            wrapper.parent().css("padding-left","360px") ;  
            var testHTML = jQuery('.view-display-id-page_casestudies').parent().html();
            var cnt=0;
            var paginatorHTML='';
            var paginator='';
            jQuery(".view-display-id-page_casestudies  .view-content .views-row").each(function() {
                 if (cnt==0) { 
                     paginatorHTML=paginatorHTML+"<div class = 'views-row selected'></div>";
                 } else {
                    paginatorHTML=paginatorHTML+"<div class = 'views-row'></div>"; 
                 }
                 cnt=cnt+1;
             })
             
             
             var countAll = '<div class="paginator_counter"><span class="count">1</span> of '+cnt+'</div>';
             paginator=paginator+"<div class = 'slideSelectors'><div class='page-title'>Title here</div>";
             paginator=paginator+countAll+paginatorHTML;
             paginator=paginator+"</div>";
             
             
             
             jQuery('.view-display-id-page_casestudies').parent().html(testHTML+paginator);
             
             
             /* case studies carousel */
             jQuery('.view-display-id-page_casestudies').iosSlider({
                                            desktopClickDrag: true,
                                            snapToChildren: true, 
                                            autoSlide: true,
                                            scrollbarMargin: '0',
                                            scrollbarBorderRadius: '0',
                                            scrollbar: true,
                                            navSlideSelector: '.slideSelectors .views-row',
                                            onSlideChange: slideChange,
                                            onSliderLoaded:slideStart,
                                            infiniteSlider: true,
                                            WIDTH:228 
            });
             
            
            
            
            var viewBlock = jQuery('.view-display-id-block_testimonials').parent();
            
            viewBlock.css("background","url('https://oilandgas.page.com/sites/all/themes/mp/css/images/testimonials/testimonials_back.jpg')");
            viewBlock.css("width","483px");
            viewBlock.css("height","208px");
            viewBlock.css("padding-left","120px");
            viewBlock.css("position","relative");
            
            var addIn = jQuery('.view-display-id-block_testimonials').parent().html();            
            var buttons  = '<div class="testimonialsPrevious"></div><div class="testimonialsNext"></div>';
            
            jQuery('.view-display-id-block_testimonials').parent().html(addIn+buttons);
            
            
            /* testimonials carousel */                      
            jQuery('.view-display-id-block_testimonials').iosSlider({
                                            desktopClickDrag: true,
                                            snapToChildren: true, 
                                            autoSlide: false,
                                            scrollbarMargin: '0',
                                            scrollbarBorderRadius: '0',

                                            navPrevSelector: jQuery('.testimonialsPrevious'),
                                            navNextSelector: jQuery('.testimonialsNext') 
            });
            
            
            
            
             
        }
  
                                
        function slideStart(args) {				
            jQuery('.page-title').html(jQuery('.view-display-id-page_casestudies .view-content .views-row .views-field-title').html());
             var title = jQuery('.page-title .field-content a').html();
             jQuery('.page-title .field-content a').html("Case Study:<br/>"+title); 
	}                       
                                
        function slideChange(args) {
              
              var link = jQuery(args.currentSlideObject).children('.views-field-title a').text();
              jQuery(args.currentSlideObject).children('.views-field-title a').html("hello"+link);
              jQuery('.page-title').html(jQuery(args.currentSlideObject).children('.views-field-title').html());              
              var title = jQuery('.page-title .field-content a').html();
              jQuery('.page-title .field-content a').html("Case Study:<br/>"+title);
              jQuery('span.count').html(args.currentSlideNumber);
	      jQuery('.slideSelectors div.selected').removeClass('selected');
	      jQuery('.slideSelectors div.views-row:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
	}
        
        
        if (jQuery('.url-front-page .view-display-id-page').length>0) {            
             var paginatorHTML='';
             var paginator='';
             var cnt=0;
             jQuery(".view-display-id-page  .view-content .views-row").each(function() {
                 if (cnt==0) { 
                     paginatorHTML=paginatorHTML+"<div class = 'views-row selected'></div>";
                 } else {
                    paginatorHTML=paginatorHTML+"<div class = 'views-row'></div>"; 
                 }
                 cnt=cnt+1;
             })
             paginator=paginator+"<div class='slideSelectors-home'>";
             paginator=paginator+paginatorHTML;
             paginator=paginator+"</div>";
             
             
            var frontALL = jQuery('.url-front-page .view-display-id-page').parent().html();
            var containers = '<div id="carousel-item-title">Title here</div><div id="paginator">'+paginator+'</div>';
            jQuery('.url-front-page .view-display-id-page').parent().html(containers + frontALL);
            
            
            jQuery('.url-front-page .view-display-id-page').iosSlider({
                                            desktopClickDrag: true,
                                            snapToChildren: true, 
                                            autoSlide: true,
                                            scrollbarMargin: '0',
                                            scrollbarBorderRadius: '0',
                                            onSlideChange:getHomeTitleChange,
                                            navSlideSelector: '.slideSelectors-home .views-row',
                                            onSliderLoaded:getHomeTitle
            });
            
            
           
        }

         function getHomeTitleChange(args) {
              jQuery('#carousel-item-title').html(jQuery(args.currentSlideObject).children('.views-field-title').text());             // 
	      jQuery('.slideSelectors-home div.selected').removeClass('selected');              
	      jQuery('.slideSelectors-home div.views-row:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
              
              
            }   
            
            function getHomeTitle(args) {              
              jQuery('#carousel-item-title').html(jQuery(args.currentSlideObject).children('.views-field-title').text());
              jQuery('.slideSelectors-home div.selected').removeClass('selected');
	      jQuery('.slideSelectors-home div.views-row:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
            }   
            
         /* get intouch */
         
         if ((jQuery(".page-get-in-touch .block-mp-findoffice-mp-findoffice-findoffice").length>0) || jQuery(".context-office .block-mp-findoffice-mp-findoffice-findoffice").length>0) {
             var all = jQuery(".block-mp-findoffice-mp-findoffice-findoffice").html();
             var intro = '<p class="intro">Michael Page Oil & Gas has an established global presence, with a network of offices across the major oil and gas hubs. Choose your region and country of interest below to get in touch with your local recruitment team.</p>';
             jQuery(".block-mp-findoffice-mp-findoffice-findoffice").html(intro+all)
            
         }
         
        if (jQuery(".block-mp-findoffice-mp-findoffice-findoffice").length>0) {
            jQuery(".item-list ul li ul li").each(function() {
                jQuery(this).attr("class","submenu");
            })
         }     
            
            
         jQuery(".block-mp-findoffice-mp-findoffice-findoffice .item-list ul > li").click(function() {
              
              
              
              
              
//              if (jQuery(this).parent().attr("class")=="submenu") {
//                alert("submenu");
//              }


              if (jQuery(this).attr("class")!="submenu"){
                if (jQuery(this).hasClass("selected")) {

                } else {   

                      jQuery(".block-mp-findoffice-mp-findoffice-findoffice .item-list ul li .item-list ul").slideUp();
                      jQuery(".block-mp-findoffice-mp-findoffice-findoffice .item-list ul li").removeClass("selected");
                      jQuery(this).find(".item-list ul").slideDown();
                      jQuery(this).addClass("selected");
                }
              }
               
         })
         
         if (jQuery(".block-mp-findoffice-mp-findoffice-findoffice .item-list ul li").length>0) {
            
            
             
             jQuery(".block-mp-findoffice-mp-findoffice-findoffice .item-list ul li").each(function() { 
                var check =jQuery(this).text();
                if (check=="Other") {
                    jQuery(this).remove();
                }
             })
         }
         /* section renders the map and regions and creates click functionality for regions to show locations */
         
         if (jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice ul li").length>0) {
        
            var myObjectRegions   = [];
            var myObjectAll = new Array(); 
          
            
           
            jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice div > div > div.item-list > ul > li").each(function() {   
                 
                 var myObjectLocations = []; 
                 
                 var regions    = jQuery(this).clone(); 
                 var locations  = jQuery(this).clone();
                 
                 jQuery('div.item-list').remove();
                 var r_text     = regions.text();
                 myObjectAll[r_text]    = new Array();
                 jQuery(locations).find(".item-list ul > li").each(function() {   
                     var check = jQuery(this).text();
                     
                     if (check!="Other") {
                     myObjectAll[r_text].push(jQuery(this).text());
                     }
                 });      
                 myObjectRegions.push(regions.text());                   
            })
 
            // create html regions list 
            var regionsHTML = '<div id="regions-html"><p>To view details of our offices, please click on a point of the map.<br/><br/>Regions:<ul>';
            
//            for (i = 0; i < myObjectRegions.length; ++i) { 
//                if (myObjectRegions[i]!="Other") {
                    
//                    if ( myObjectRegions[i]=="AfricaAfrica") {
//                        regionsHTML = regionsHTML + '<li>Africa</li>';
//                    } else {
//                        alert(myObjectRegions[i])
//                        regionsHTML = regionsHTML + '<li>'+ myObjectRegions[i] + '</li>';
//                    }
 
//                }
             regionsHTML = regionsHTML + '<li>Africa</li><li>Asia Pacific</li><li>Europe</li><li>Latin America</li><li>Middle East</li><li>North America</li>'

            
            regionsHTML = regionsHTML + '</ul></div><div id="locations-container-html"></div>';
            
            // create the hover links 
            var hoversHTML = '';            
            for (i = 0; i < myObjectRegions.length; ++i) { 
                
                hoversHTML = hoversHTML + '<a href="" class="map-'+jQuery.trim(myObjectRegions[i].replace(" ", "_"))+'">'+jQuery.trim(myObjectRegions[i])+'</a>';
            }      
          
            jQuery('<div class="link-wrapper">').insertBefore(jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice"));
            
            var links = '<a class="map-AfricaAfrica" href="">Africa</a><a class="map-Asia_Pacific" href="">Asia Pacific</a><a class="map-Europe" href="">Europe</a><a class="map-Latin_America" href="">Latin America</a><a class="map-Middle_East" href="">Middle East</a><a class="map-North_America" href="">North America</a>'; 
            var it ='<div id="locations-container-html"></div>';
//             jQuery('.link-wrapper').html(hoversHTML+it)
             
            var allMap = jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice").html();
            jQuery(".link-wrapper").html(links + allMap+ it);            
            // replace rendered list with revised links 
            jQuery("#locations-container-html").html(regionsHTML);
            // change the map state if region clicked and list locations from array myObjectAll
            jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice div div div.item-list").remove()
            jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice").remove(); 
            
         
            
             jQuery(".link-wrapper a").live("click",function() { 
               
               if (jQuery(this).attr("class")!="map-location") {               
                    var selectedRegion=jQuery(this).text(); 
                    var requiredLocations = myObjectAll[selectedRegion];
                    var locationsHTML = '<a href="" id="close-locations"></a><div id="locations-html">'+selectedRegion+'</strong></p>'+cntryText[selectedRegion]+'';

    //                for (i = 0; i < requiredLocations.length; ++i) {                        
    //                       locationsHTML = locationsHTML + '<li><a href="/office/'+requiredLocations[i]+'" class="map-location">'+ requiredLocations[i] + '</a></li>';
    //                       alert(requiredLocations[i])
    //                 
    //                 }   
                    locationsHTML = locationsHTML + '</div>';
                    // put the location into container and remove region list
                    jQuery("#regions-html").html("display","none");
                    jQuery("#locations-container-html").html(locationsHTML);
    //                jQuery("#regions-html").css("display","none");
                    var container = jQuery(".link-wrapper");
                    var region = jQuery(this).text();
                    if (region=="Africa Africa") { region="Africa"; }
                    container.css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/looking/"+region+".jpg')  0 20px no-repeat");
                    return false;
               }
            })
            
            // revert the map to original state if the user closes locations
            jQuery("a#close-locations").live("click",function() { 
//                // show original region list hidden in a.maphover click
//                  jQuery("#regions-html").css("display","block");
//                // remove locations and close link
//                jQuery("a#close-locations").remove();
                  jQuery("#locations-container-html").html(regionsHTML);
//                // update the map
//                var container = jQuery(".url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice");
                var container = jQuery(".link-wrapper");
                container.css("background","#fff url('https://oilandgas.page.com/sites/all/themes/mp/css/images/looking/map_back.jpg')  0 20px no-repeat");
                return false;
            })
            
 
         } // end .url-looking-to-hire .block-mp-findoffice-mp-findoffice-findoffice ul li.length>0
         
         
         
         
         /* NEWS EVENTS AND INSIGHTS */
         
         if (jQuery(".node-events").length>0) {
             
             // reposition the image and title
             var articletitle = jQuery("h1.title");             
             var articleImage = jQuery(".field-name-field-nei-image");
             articleImage.insertBefore(jQuery(".field-name-body"));
             articletitle.insertBefore(jQuery('.field-name-body .field-items'));
             // create a back button
//             var  button = jQuery('<a href="/market-updates/all" class="" id="mu_back">').insertBefore(articletitle); 
             var regions = jQuery(".field-name-field-tax-regions");
             var eventDate = jQuery(".field-name-field-event-date");
             jQuery("h1.title").html(articletitle.text()+'<a href="/market-updates/all" class="mu_back"></a>');
             regions.insertAfter(articletitle);
             eventDate.insertAfter(regions);
           
         }
         /* reposition the image and title */
         if (jQuery(".node-news").length>0) {
             var articletitle = jQuery("h1.title");     
             articletitle.insertBefore(jQuery('.field-name-body .field-items'));
             jQuery("h1.title").html(articletitle.text()+'<a href="/market-updates/all" class="mu_back"></a>');          
         }
         
         if (jQuery(".node-insights").length>0) {             
              // reposition the image and title
             var articletitle = jQuery("h1.title");     
             articletitle.insertBefore(jQuery('.field-name-body .field-items'));
              jQuery("h1.title").html(articletitle.text()+'<a href="/market-updates/all" class="mu_back"></a>');
             var regions = jQuery(".field-name-field-tax-regions"); 
             regions.insertAfter(articletitle);
         }
         
         
         if (jQuery(".node-type-office-country  .node-office-country").length>0) {            
             var location = jQuery("h1.title");
             location.insertBefore(jQuery(".node-office-country .content"));
             var loca = location.text();
             location.text("Michael Page Oil & Gas, "+loca);
             ff = jQuery(".submitted");
             ff.insertAfter(jQuery(".node-office-country .field-name-body"));
             
             jQuery(".item-list ul li ul li a").each(function() {
                
                  
                 if (jQuery(this).text()==loca) {
                    
                    jQuery(this).parent().parent().slideDown();
                    jQuery(this).css("color","#fb7204");
                }
             })
             
             
             
         }
        
         /* footer stuff */         
         if (jQuery(".region-footer-first").length>0) {             
             /* add the click bar */
             var footer = jQuery(".region-footer-first .region-inner");
             var click = jQuery('<div class="footer-click">').insertBefore(footer);
             jQuery('.block-mp-logo-logo').remove();
             var withLogo = jQuery('.footer-click').html();
             var clickDown = '<img src="https://oilandgas.page.com/sites/all/themes/mp/footer_logo.jpg"><div class="expand"><a href=""  class="close">Expand footer</a></div>';
             jQuery('.footer-click').html(clickDown);        
             /* wrap the menus so as can get the footer width correct and not mess with the grid */
             jQuery('#zone-footer .region-footer-first .region-footer-first-inner').wrap('<div id="footer-link-container">');
        
        }
         if (jQuery(".page-looking-job .ui-slider-horizontal").length>0) {               
              jQuery(".ui-slider-horizontal").wrap('<div class="slider-wrap">');
              jQuery(".ui-slider-horizontal").before('<div class="title">Salary range</div>');
              var first=jQuery('input#edit-field-job-min-sal');
              
              jQuery("a.ui-slider-handle").each(function() {                   
                jQuery(this).html(first);                 
                first = jQuery('input#edit-field-job-max-sal');                
              })             
            }
            
            /* dump the footer on the end of region content on the looking for a job page cause of the relative dynamic container 
             * maintaing the job display.... different layout to rest of site.
             */
            if (jQuery(".url-looking-job .region-content").length>0) {  
                var footer =  jQuery(".section-footer");            
                footer.insertAfter(jQuery(".content-container"))
            }
            
            if (jQuery(".expand a").length>0) {
               
                jQuery(".expand a").live("click",function() {
                    var toSlide      = jQuery("#footer-link-container");
                    var tolink       = jQuery(".expand a");
                    var toSlideClass = tolink.attr("class"); 
                    
                    if (toSlideClass=="open") {
                         tolink.removeClass("open");;
                         tolink.addClass("close");                        
                          jQuery("#footer-link-container").slideUp('slow', function() {
                            tolink.html("Expand footer");
                            tolink.css("background", "url('https://oilandgas.page.com/sites/all/themes/mp/css/images/footer/expand_click.jpg') no-repeat scroll right center transparent");
                         });
                    } else {
                         tolink.removeClass("close");;
                         tolink.addClass("open");
//                         return false;
                         jQuery("#footer-link-container").slideDown('slow', function() {
                            
                             tolink.html("Close footer");
                            tolink.css("background", "url('https://oilandgas.page.com/sites/all/themes/mp/css/images/footer/close_click.jpg') no-repeat scroll right center transparent");
                         jQuery('html, body').animate({scrollTop: jQuery("body").height()}, 800);
                         });
                    }
                    return false;
                })
            }
           if (jQuery(".url-market-updates").length>0) {
               
           
               
               jQuery('<div class="survey-info">').insertAfter(jQuery(".view-display-id-block_survey_curr"));
               jQuery('.survey-info').html("<p>We'll be asking a series of questions relating to the global oil and gas market.</p><p>You can view all of the results as they become available by referring back to this page.</p><p>The results of our first poll are due soon – have you voted yet?</p>")
           
             }
            if (jQuery("textarea").length>0) {
                
                jQuery("textarea").focus(function() {
                 
                   jQuery(this).val('');
                })
                
            }
       
            if (jQuery("#block-menu-menu-menu-footer-center").length>0) {
                jQuery("#block-menu-menu-menu-footer-center .menu li a").each(function() {
                    jQuery(this).attr("target","_newFrame");
                })
            }
           
});

 