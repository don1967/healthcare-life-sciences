<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 
/**
 * Implements hook_preprocess_views_view()
 */
function mp_preprocess_views_view(&$vars) {
  
  if($vars['name'] == 'job_search_page')  {
    drupal_add_library('system','ui.slider');
  }
}
 
/**
 * Implements hook_preprocess_html()
 * Implemented here and not in preprocess folder to keep inline with Drupal best
 * practise rather than Omega
 */
function mp_preprocess_html(&$vars) {
  global $base_url, $base_path;
  if(module_exists('path')) {
    $alias = drupal_get_path_alias(check_plain($_GET['q']));
    if($alias != $_GET['q']) {
      $path = $alias;
    }
    else {
      $path = check_plain($_GET['q']);
    }
  }
 
  $class = 'url-'.str_replace('/','-',$path); 
    $vars['attributes_array']['class'][] = $class;
}
 
/**
 * Implements hook_preprocess_page()
 * Implemented here and not in preprocess folder to keep inline with Drupal best
 * practise rather than Omega
 */
function mp_preprocess_page(&$variables, $hook) {
   
  drupal_add_css(path_to_theme() . '/css/ie-lte-9.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 9', '!IE' => FALSE), 'preprocess' => FALSE, 'weight' => 20));
   drupal_add_css(path_to_theme() . '/css/ie-lte-8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE, 'weight' => 21));
    drupal_add_css(path_to_theme() . '/css/ie-lte-7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE, 'weight' => 22));
     
  global $base_url;
  $vars['base_url'] = $base_url;
  

  
  // Page template suggestions based off of content types
  if (isset($variables['node'])) { 
    $variables['theme_hook_suggestions'][] = 'page__node_'. $variables['node']->type;
  } else {
    if (drupal_is_front_page()) {
      drupal_set_title('Homepage');
    }
  }
}

/**
 * Implements hook_delta_blocks_breadcrumb().
 * Adds a breadcrumb and works out if a trail is needed.
 */
 
function mp_delta_blocks_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $breadcrumb[] = l(drupal_get_title(), current_path());
  
  // Provide a navigational heading to give context for breadcrumb links to
  // screen-reader users. Make the heading invisible with .element-invisible.
  $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
  
  $output .= '<div class="breadcrumb">' . implode(' &raquo; ', $breadcrumb) . '</div>';
  return $output;
}

function custom_theme_preprocess_html(&$variables) {
 
  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie-lte-9.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie-lte-8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie-lte-7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
 }
 
